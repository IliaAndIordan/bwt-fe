import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material/grid-list';
import { RoutedComponents, BookRoutingModule } from './routing.module';
import { SharedModule } from '../@share/shared.module';
import { BwtMaterialModule } from '../@share/material/material.module';
import { PageHeaderComponent } from './page-header/page-header.component';
import { BookComponent } from './book.component';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BwtMaterialModule,
    MatGridListModule,
    BookRoutingModule,
  ],
  declarations: [
    RoutedComponents,
    PageHeaderComponent,
  ],
  exports: [
    PageHeaderComponent,
  ]
})
export class BookModule { }
