import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
// Constants
// Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { AuthService } from 'src/app/@core/auth/api/auth.service';
import { BookModel } from 'src/app/@core/api/book/dto';
import { BwtRoutes } from 'src/app/@core/const/bwt-routes.const';
import { StorageKeysLocal } from 'src/app/@core/const/bwt-storage-key.const';

@Component({
    selector: 'app-page-book-header',
    templateUrl: './page-header.component.html',
})

export class PageHeaderComponent implements OnInit, OnDestroy {

    user: UserModel;
    userChanged: any;
    isAdmin: boolean;

    book: BookModel;

    constructor(
        private router: Router,
        private cus: CurrentUserService,
        private authService: AuthService) { }

    ngOnInit(): void {
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
        this.getSelBook();
        this.userChanged = this.cus.UserChanged.subscribe(newUser => {
            // console.log('newUser->', newUser);
            this.user = newUser;
            this.isAdmin = this.cus.isAdmin;
            this.getSelBook();
        });


    }

    getSelBook() {
        const bookJson = localStorage.getItem(StorageKeysLocal.SelBook);
        if (bookJson) {
            this.book = BookModel.fromJSON(JSON.parse(bookJson));
        } else {
            this.book = undefined;
        }
    }

    ngOnDestroy(): void {
        if (this.userChanged) {
            this.userChanged.unsubscribe();
        }
    }

    //#region Navigation

    gotoAircraftAdmin() {
        if (this.cus.user && this.cus.isAdmin) {
            this.router.navigate(['/', BwtRoutes.Dashboard]);
        } else {
            this.logout();
        }

    }

    logout(): void {
        this.authService.logout();
    }


    //#endregion


}
