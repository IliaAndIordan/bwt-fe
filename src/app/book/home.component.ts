import { Component, ViewContainerRef, OnInit, OnDestroy } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { CurrentUserService } from '../@core/auth/current-user.service';
import { BookModel } from '../@core/api/book/dto';
import { BookService } from '../@core/api/book/book.service';
import { UserModel } from '../@core/auth/api/dto';
import { StorageKeysLocal } from '../@core/const/bwt-storage-key.const';
import { AutorService } from '../@core/api/user/autor.service';
import { AutorModel } from '../@core/api/user/dto';
import { Router } from '@angular/router';
import { BwtRoutes } from '../@core/const/bwt-routes.const';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})

export class HomeComponent implements OnInit, OnDestroy {



  colspan4Card = 3;
  colspanGridMainBoard = 8;
  colspanGridMainRSide = 4;
  bpoObsHandset: any;
  bpoObsTablet: any;
  bpoObsWeb: any;
  bpoObssmall: any;

  user: UserModel;
  userChanged: any;
  isAdmin: boolean;

  book: BookModel;
  autor: AutorModel;

  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Card 1', cols: 1, rows: 1 },
          { title: 'Card 2', cols: 1, rows: 1 },
          { title: 'Card 3', cols: 1, rows: 1 },
          { title: 'Card 4', cols: 1, rows: 1 }
        ];
      }

      return [
        { title: 'Card 1', cols: 2, rows: 1 },
        { title: 'Card 2', cols: 1, rows: 1 },
        { title: 'Card 3', cols: 1, rows: 2 },
        { title: 'Card 4', cols: 1, rows: 1 }
      ];
    })
  );

  isAuthenticated: boolean;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private vcr: ViewContainerRef,
    private cus: CurrentUserService,
    private bookService: BookService,
    private autorService: AutorService) { }

  ngOnInit(): void {
    this.isAuthenticated = this.cus.isAuthenticated;
    this.user = this.cus.user;
    this.isAdmin = this.cus.isAdmin;
    this.getSelBook();
    this.userChanged = this.cus.UserChanged.subscribe(newUser => {
      //console.log('newUser->', newUser);
      this.user = newUser;
      this.isAdmin = this.cus.isAdmin;
      if (this.cus.isAuthenticated) {
        this.getSelBook();
      } else {
        this.router.navigate(['/', BwtRoutes.Public]);
      }
    });
    this.breakpointObserverInit();
    // this.loadBookList();
  }

  ngOnDestroy(): void {
    if (this.userChanged) { this.userChanged.unsubscribe(); }
    if (this.bpoObsHandset) { this.bpoObsHandset.unsubscribe(); }
    if (this.bpoObsTablet) { this.bpoObsTablet.unsubscribe(); }
    if (this.bpoObsWeb) { this.bpoObsWeb.unsubscribe(); }
    if (this.bpoObsHandset) { this.bpoObsHandset.unsubscribe(); }
  }

  getSelBook() {
    // console.log('getSelBook-> isAuthenticated:', this.cus.isAuthenticated);
    if (!this.cus.isAuthenticated) {
      this.router.navigate(['/', BwtRoutes.Public]);
    }

    this.book = this.cus.selectedBook;

    if (this.book) {
      this.autorService.autor(this.book.userId)
        .subscribe((autorList: Array<AutorModel>) => {
          if (autorList) {
            this.autor = autorList[0];
          } else {
            this.autor = undefined;
          }
        });
    }

  }


  breakpointObserverInit(): void {
    this.bpoObsHandset = this.breakpointObserver
      .observe([Breakpoints.Handset])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Handset viewport  mode');
          this.colspan4Card = 12;
          this.colspanGridMainBoard = 10;
          this.colspanGridMainRSide = 2;
        }
      });
    this.bpoObsTablet = this.breakpointObserver
      .observe([Breakpoints.Tablet])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Tablet viewport mode');
          this.colspan4Card = 6;
          this.colspanGridMainBoard = 9;
          this.colspanGridMainRSide = 3;
        }
      });

    this.bpoObsWeb = this.breakpointObserver
      .observe([Breakpoints.Web])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Web viewport mode');
          this.colspan4Card = 3;
          this.colspanGridMainBoard = 8;
          this.colspanGridMainRSide = 4;
        }
      });
    this.bpoObssmall = this.breakpointObserver
      .observe([Breakpoints.Small])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Small viewport  mode');
          this.colspan4Card = 12;
          this.colspanGridMainBoard = 12;
          this.colspanGridMainRSide = 0;
        }
      });

  }

  updateFields(): void { }

  //#region Data
  /*
    loadBookList(): void {
      this.bookList = new Array<BookModel>();
      this.bookService.bookList()
        .subscribe((resp: Array<BookModel>) => {
          this.bookList = resp;
          console.log('loadBookList->', resp);
          this.updateFields();
        });

    }

    reloadBookList(): void {
      this.bookService.resetBookCache();
      this.loadBookList();
    }
  */
  //#endregion
}
