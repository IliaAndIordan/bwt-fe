import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// --- Constants

// Local Components
import { HomeComponent } from './home.component';
import { BookComponent } from './book.component';

const routes: Routes = [
    {path: '', component: HomeComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class  BookRoutingModule { }

export const RoutedComponents = [ BookComponent, HomeComponent];
