import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';


// Custom Modules
import { BwtMaterialModule } from './@share/material/material.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BwtCoreModule } from './@core/core.module';
import { AppRoutingModule, routableComponents } from './app-routing.module';
import { ToastrModule } from 'ngx-toastr';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ModalModule } from './@share/modal/modal.module';
import { SharedModule } from './@share/shared.module';

declare module "@angular/core" {
  interface ModuleWithProviders<T = any> {
    ngModule: Type<T>;
    providers?: Provider[];
  }
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgProgressModule.withConfig({
      spinner: false,
      color: '#f71cff',
      ease : 'linear',
      debounceTime: 0,
      fixed : false,
    }),
    NgProgressHttpModule,
    // Custom Modules
    BwtCoreModule,
    SharedModule,
    AppRoutingModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-top-center',
      preventDuplicates: true,
    }),
  ],
  declarations: [
    AppComponent,
    routableComponents,
  ],

  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent, routableComponents]

})
export class AppModule { }
