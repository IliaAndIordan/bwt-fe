import { NgModule } from '@angular/core';
// Local Modules
import { SharedModule } from '../@share/shared.module';
import { RoutedComponents, PublicRoutingModule } from './routing.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Components

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PublicRoutingModule,
        //
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        RoutedComponents,
        //
    ],
    exports: [
    ],
    entryComponents: [

    ]
})
export class PublicModule { }

