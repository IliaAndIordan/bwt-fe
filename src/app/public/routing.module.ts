import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// --- Constants

// Local Components
import { HomeComponent } from './home.component';
import { PublicComponent } from './public.component';

const routes: Routes = [
    {path: '', component: HomeComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class  PublicRoutingModule { }

export const RoutedComponents = [ PublicComponent, HomeComponent];
