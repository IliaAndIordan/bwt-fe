import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { MatSidenav } from '@angular/material/sidenav';
import { BwtNavSideLeft } from 'src/app/@core/const/bwt-nav-side-left.const';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AuthService } from 'src/app/@core/auth/api/auth.service';

@Component({
  selector: 'app-bwt-nav-list-item-left',
  templateUrl: './bwt-nav-list-item-left.component.html',
  styleUrls: ['./bwt-nav-list-item-left.component.scss'],
})
export class BwtNavListItemLeftComponent implements OnInit, OnDestroy {


  // @ViewChild('drawer') drawer: MatSidenav;
  navigation = BwtNavSideLeft;
  user: UserModel;
  isAuthenticated: boolean;
  userChangedSubsc: any;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
    private router: Router,
    private route: ActivatedRoute,
    private cus: CurrentUserService,
    private authService: AuthService) { }


  ngOnInit(): void {
    this.user = this.cus.user;
    this.isAuthenticated = this.cus.isAuthenticated;
    this.userChangedSubsc = this.cus.UserChanged.subscribe((user: UserModel) => {
      this.onUserChanged();
    });
    this.onUserChanged();
  }

  ngOnDestroy(): void {
    if (this.userChangedSubsc) { this.userChangedSubsc.unsubscribe(); }
  }

  public onUserChanged(): void {
    this.user = this.cus.user;
    this.isAuthenticated = this.cus.isAuthenticated;
    this.navigation = BwtNavSideLeft.filter(x =>
      (x.userRole < ((this.user ? this.user.role : 1) + 1) && (x.authorised === this.isAuthenticated)));
    // console.log('app-ams-lnav-list-items.onUserChanged: isAuthenticated=', this.isAuthenticated);
  }

  public isVisible(item) {
    let visible = true;
    if (item.authorised !== 'undefined') {
      visible = (item.authorised === this.cus.isAuthenticated);
    }

    return visible;
  }


  logout(): void {
    this.authService.logout();
  }


}
