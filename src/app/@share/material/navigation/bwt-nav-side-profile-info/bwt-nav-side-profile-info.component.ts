import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from '../../../../@core/auth/current-user.service';
import { UserModel } from '../../../../@core/auth/api/dto';
import { LocalUserSettings } from '../../../../@core/auth/local-user-settings';
import { AVATAR_IMAGE_URL } from 'src/app/@core/const/bwt-storage-key.const';


@Component({
  selector: 'app-bwt-nav-side-profile-info',
  templateUrl: './bwt-nav-side-profile-info.component.html',
  styleUrls: ['./bwt-nav-side-profile-info.component.scss']
})

export class BwtNavSideProfileInfoComponent implements OnInit {

  public snavLeftWidth = 4;
  public currUser: UserModel;
  public paddingBottom = 0;
  public maxHeight = 64;
  public paddingTop = 12;
  public heightPx = 64;
  public minHeight = 64;
  avatarImageUrl: string;

  constructor(private currUserService: CurrentUserService) {

    this.snavLeftWidth = 200; // this.currUserService.localSettings.snavLeftWidth;

    this.avatarImageUrl = AVATAR_IMAGE_URL;
    this.onUserChanged();
    this.currUserService.UserChanged.subscribe((user: UserModel) => {
      this.onUserChanged();
    });

    this.currUserService.LocalSettingsChanged.subscribe((settings: LocalUserSettings) => {
      this.onSettingsChanged();
    });
    this.onSettingsChanged();
  }

  ngOnInit() {
  }

  onUserChanged() {

    if (this.currUserService.isAuthenticated) {
      this.currUser = this.currUserService.user;
      this.avatarImageUrl = this.currUserService.avatarUrl;
    } else {
      this.currUser = undefined;
      this.avatarImageUrl = AVATAR_IMAGE_URL;
    }

  }

  onSettingsChanged() {
    this.snavLeftWidth = 200; // this.currUserService.localSettings.snavLeftWidth;
    this.paddingBottom = this.snavLeftWidth > 6 ? 64 : 0;
    this.maxHeight = this.snavLeftWidth > 6 ? 136 : 64;
    this.paddingTop = this.snavLeftWidth > 6 ? 24 : 12;
    this.heightPx = this.snavLeftWidth > 6 ? 136 : 64;
    this.minHeight = this.snavLeftWidth > 6 ? 136 : 64;

  }

}
