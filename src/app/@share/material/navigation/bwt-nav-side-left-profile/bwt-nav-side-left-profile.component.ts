import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CurrentUserService } from '../../../../@core/auth/current-user.service';
import { UserModel } from '../../../../@core/auth/api/dto';
import { AVATAR_IMAGE_URL } from 'src/app/@core/const/bwt-storage-key.const';

@Component({
  selector: 'app-bwt-nav-side-left-profile',
  templateUrl: './bwt-nav-side-left-profile.component.html',
  styleUrls: ['./bwt-nav-side-left-profile.component.scss']
})
export class BwtNacSideLeftProfileComponent implements OnInit {

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClick: EventEmitter<any> = new EventEmitter<any>();

  public user: UserModel;
  private userChangedSubscription: any;

  avatarImageUrl: string;

  constructor(private currUserService: CurrentUserService) {
    this.onUserChanged();
    this.currUserService.UserChanged.subscribe((user: UserModel) => {
      this.onUserChanged();
    });
  }

  ngOnInit() {
  }

  onUserChanged() {

    if (this.currUserService.isAuthenticated) {
      this.user = this.currUserService.user;
      this.avatarImageUrl = this.currUserService.avatarUrl;
    } else {
      this.user = undefined;
      this.avatarImageUrl = AVATAR_IMAGE_URL;
    }

  }


  public get title(): string {
    let rv = 'Login';
    if (this.user) {
      rv = this.user.name ? this.user.name : this.user.eMail;
    }
    return rv;
  }

  handleClick(event: any) {
    // what we sending to the event handler that is being passed to parent function
    this.onClick.emit('emit');
  }

}
