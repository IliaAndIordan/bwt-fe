import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BwtNavToolbarComponent } from './bwt-nav-toolbar/bwt-nav-toolbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatRadioModule } from '@angular/material/radio';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BwtNavListItemLeftComponent } from './bwt-nav-list-item-left/bwt-nav-list-item-left.component';
import { BwtNacSideLeftProfileComponent } from './bwt-nav-side-left-profile/bwt-nav-side-left-profile.component';
import { BwtNavSideProfileInfoComponent } from './bwt-nav-side-profile-info/bwt-nav-side-profile-info.component';
import { RegisterModalDialog } from './modal/register/register-modal.dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatRadioModule,
    MatFormFieldModule,
    MatDialogModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  declarations: [
    BwtNavToolbarComponent,
    BwtNavListItemLeftComponent,
    BwtNacSideLeftProfileComponent,
    BwtNavSideProfileInfoComponent,
    RegisterModalDialog,
  ],
  exports: [
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    BwtNavToolbarComponent,
    BwtNavListItemLeftComponent,
    BwtNacSideLeftProfileComponent,
    BwtNavSideProfileInfoComponent,
    RegisterModalDialog,
  ],
  entryComponents: [
    RegisterModalDialog
  ]
})
export class NavigationModule { }
