import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { AuthService } from '../../../@core/auth/api/auth.service';
import { BwtRoutes } from 'src/app/@core/const/bwt-routes.const';
import { ResponseAuthenticate, UserModel } from 'src/app/@core/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
// Constants

export interface LoginData {
  email: string;
  password: string;
}

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.dialog.html',
  styleUrls: ['./login-modal.dialog.scss']
})

// tslint:disable-next-line:component-class-suffix
export class LoginModalDialog implements OnInit {

  formGrp: FormGroup;

  email: string;
  password: string;

  hasSpinner = false;
  errorMessage: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private cus: CurrentUserService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<LoginModalDialog>,
    @Inject(MAT_DIALOG_DATA) public data: LoginData) {
    this.email = data.email;
    this.password = data.password;
  }

  get ctr_email() { return this.formGrp.get('ctr_email'); }
  get ctr_password() { return this.formGrp.get('ctr_password'); }

  get errorEMail(): string {
    return this.ctr_email.hasError('required') ? 'E-Mail is required.' :
      this.ctr_email.hasError('email') ? 'Please enter a valid email address' : '';
  }

  get errorPassword(): string {
    return this.ctr_password.hasError('required') ? 'Password is required.' :
      this.ctr_password.hasError('minlength') ? 'Password must be et least 3 characters lon' : '';
  }

  ngOnInit(): void {

    this.errorMessage = undefined;
    this.hasSpinner = false;

    this.formGrp = this.fb.group({
      ctr_email: new FormControl(this.email, [Validators.required, Validators.email]),
      ctr_password: new FormControl(this.password, [Validators.required, Validators.minLength(3)]),
    });
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  onLoginClick() {

    if (this.formGrp.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;

      this.email = this.ctr_email.value;
      this.password = this.ctr_password.value;

      this.authService.autenticate(this.email, this.password)
        .subscribe((res: ResponseAuthenticate) => {
         
          const user = this.cus.user;
           // console.log('autenticate-> resp user', user);
          if (user && user.eMail === this.email) {
            // this.spinerService.display(false);
            this.hasSpinner = false;
            this.toastr.success('Welcome ' + user.name + '!', 'Login success');
            setTimeout((router: Router) => {
              this.router.navigate(['/', BwtRoutes.Dashboard]);
              this.dialogRef.close(user);
            }, 1000);
          } else {
            this.hasSpinner = false;
            this.toastr.error('Login Failed', 'Login Failed');
          }
        },
          err => {
            console.error('autenticate-> got an error: ' + err);
            this.errorMessage = 'Please enter valid values for fields';
            this.hasSpinner = false;
          },
          () => {
            console.log('autenticate-> got a complete notification');
          });
    } else {
      this.errorMessage = 'Please enter valid values for fields';
      this.hasSpinner = false;
    }
  }


}
