import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { BookModel, CharacterModel, ResponseCharacterEdit } from 'src/app/@core/api/book/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { BookService } from 'src/app/@core/api/book/book.service';
import { CharacterService } from 'src/app/@core/api/book/character.service';

// Constants
const Preload = 'logo-image-preload';
const Wide = 'logo-image-wide';
const Tall = 'logo-image-tall';
const NO_IMAGE = '../../../assets/images/common/noimage.png';

@Component({
  selector: 'app-edit-crtr-dialog',
  templateUrl: './edit-character.dialog.html',
})

export class EditCharacterDialogComponent implements OnInit {

  form: FormGroup;
  hasSpinner = false;

  book: BookModel;
  user: UserModel;
  character: CharacterModel;

  errorMessage: string;
  imageClass: string = Preload;
  imageUrl: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private bookService: BookService,
    private characterService: CharacterService,
    private cus: CurrentUserService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<BookModel>,
    @Inject(MAT_DIALOG_DATA) public data: CharacterModel) {
    this.character = data;
    if (this.character && this.character.book_id) {
      this.bookService.book(this.character.book_id).subscribe((res: Array<BookModel>) => {
        if (res && res.length > 0) {
          this.book = res[0];
        } else {
          this.book = this.cus.selectedBook;
        }

      });
    } else {
      this.book = this.cus.selectedBook;
    }
  }


  ngOnInit(): void {
    this.user = this.cus.user;
    this.imageUrl = (this.character && this.character.crtr_image_url) ? this.character.crtr_image_url : NO_IMAGE;
    this.form = this.fb.group({
      crtr_id: new FormControl(this.character.crtr_id, null),
      crtr_name: new FormControl(this.character.crtr_name, [Validators.required, Validators.minLength(5), Validators.maxLength(512)]),
      crtr_nikname: new FormControl(this.character.crtr_nikname, [Validators.maxLength(256)]),
      crtr_url: new FormControl(this.character.crtr_url, [Validators.maxLength(1000)]),
      crtr_desc: new FormControl(this.character.crtr_desc, [Validators.maxLength(1990)]),
      crtr_order: new FormControl(this.character.crtr_order ? this.character.crtr_order : 1, null),
      crtr_image_url: new FormControl(this.character.crtr_image_url, [Validators.maxLength(1000)]),
    });
    // this.imageUrl = (this.crtr_image_url && this.crtr_image_url.value) ? this.crtr_image_url.value : NO_IMAGE;
  }

  get crtr_name() { return this.form.get('crtr_name'); }
  get crtr_nikname() { return this.form.get('crtr_nikname'); }
  get crtr_url() { return this.form.get('crtr_url'); }
  get crtr_desc() { return this.form.get('crtr_desc'); }
  get crtr_order() { return this.form.get('crtr_order'); }
  get crtr_image_url() { return this.form.get('crtr_image_url'); }

  get errorName(): string {
    return this.crtr_name.hasError('required') ? 'Character name is required.' :
      this.crtr_name.hasError('minlength') ? 'Character name  must be et least 5 characters long' : '';
  }

  get errorMaxLenght(): string {
    return this.crtr_name.hasError('maxLength') ? 'Character name max lenght is 512 characters long.' :
      this.crtr_nikname.hasError('maxLength') ? 'Character nikname max lenght is 256 characters long.' :
        this.crtr_url.hasError('maxLength') ? 'Character URL max lenght is 1000 characters long.' :
          this.crtr_desc.hasError('maxLength') ? 'Character description max lenght is 1990 characters long' :
            this.crtr_image_url.hasError('maxLength') ? 'Character Image URL max lenght is 1000 characters long.' : '';
  }

  onNoClick(): void {
    // this.dialogRef.close();
  }

  onSubmitClick() {

    if (this.form.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;

      this.character.crtr_name = this.crtr_name.value;
      this.character.crtr_nikname = this.crtr_nikname.value;
      this.character.crtr_url = this.crtr_url.value;
      this.character.crtr_desc = this.crtr_desc.value;
      this.character.crtr_order = this.crtr_order.value;
      this.character.crtr_image_url = this.crtr_image_url.value;

      this.character.book_id = this.book.id;

      // console.log('updated book:', this.book);
      /*
      setTimeout((form: any = this) => {
        form.errorMessage = 'Airline Update Not Impllemented. ';
        form.hasSpinner = false;
      }, 300);

      */
      this.characterService.save(this.character)
        .subscribe((res: ResponseCharacterEdit) => {
          // console.log('bookService.save', resBook);
          if (res && res.status === 'success') {

            this.toastr.success('Character Updated', 'Character Updated!');
            this.hasSpinner = false;
            this.dialogRef.close(res.data.character);
          } else {
            this.errorMessage = 'Character Update Failed. ' + res.message;
            this.hasSpinner = false;
            setTimeout((router: Router) => {
              this.errorMessage = undefined;
            }, 2000);
            // this.spinerService.display(false);
            this.toastr.error('Character Update Failed', res.message);
          }

        });


    } else {
      this.errorMessage = 'Not valid input';
      this.toastr.error('Please enter valid values for fields', 'Not valid input');
    }
  }

  onImageLoad() {
    this.imageClass = this.getImageClass();
  }

  onImageError() {
    this.imageUrl = NO_IMAGE; // Don't show broken image.
  }

  getImageClass() {
    const image = new Image();
    image.src = this.imageUrl;
    if (image.width > image.height + 20 || image.width === image.height) {
      // return wide image class
      return Wide;
    } else {
      return Tall;
    }
  }

}
