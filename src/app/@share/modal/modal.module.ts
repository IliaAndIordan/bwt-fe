import { NgModule, Optional, SkipSelf } from '@angular/core';
import { BwtMaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
//
import { LoginModalDialog } from './login/login-modal.dialog';
import { EditBookDialogComponent } from './edit-book/edit-book.dialog';
import { CommonModule } from '@angular/common';
import { EditCharacterDialogComponent } from './edit-character/edit-character.dialog';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BwtMaterialModule,
    ],
    declarations: [
        LoginModalDialog,
        EditBookDialogComponent,
        EditCharacterDialogComponent,
    ],
    exports: [
        BwtMaterialModule,
        LoginModalDialog,
        EditBookDialogComponent,
        EditCharacterDialogComponent,
    ],
    entryComponents: [
        LoginModalDialog,
        EditBookDialogComponent,
        EditCharacterDialogComponent,
    ]
})

export class ModalModule {

    constructor() { }
}
