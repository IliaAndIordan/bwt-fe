import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { BookModel } from 'src/app/@core/api/book/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { BookService } from 'src/app/@core/api/book/book.service';

// Constants


@Component({
  selector: 'app-edit-book-dialog',
  templateUrl: './edit-book.dialog.html',
  styleUrls: ['./edit-book.dialog.scss']
})

export class EditBookDialogComponent implements OnInit {

  form: FormGroup;
  hasSpinner = false;

  book: BookModel;
  user: UserModel;

  errorMessage: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private bookService: BookService,
    private cus: CurrentUserService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<BookModel>,
    @Inject(MAT_DIALOG_DATA) public data: BookModel) {
    this.book = data;
  }

  ngOnInit(): void {
    this.user = this.cus.user;

    this.form = this.fb.group({
      bookTitle: new FormControl(this.book.title, [Validators.required, Validators.minLength(5), Validators.maxLength(255)]),
      bookSubTitle: new FormControl(this.book.subtitle, [Validators.maxLength(512)]),
      bookCover: new FormControl(this.book.coverpageUrl, [Validators.maxLength(1990)]),
      description: new FormControl(this.book.description, [Validators.maxLength(1990)]),
    });

  }

  get bookTitle() { return this.form.get('bookTitle'); }
  get bookSubTitle() { return this.form.get('bookSubTitle'); }
  get bookCover() { return this.form.get('bookCover'); }
  get description() { return this.form.get('description'); }

  get errorTitle(): string {
    return this.bookTitle.hasError('required') ? 'Book title is required.' :
      this.bookTitle.hasError('minlength') ? 'Book title  must be et least 5 characters long' : '';
  }

  get errorMaxLenght(): string {
    return this.bookTitle.hasError('maxLength') ? 'Book title max lenght is 255 characters long.' :
      this.bookSubTitle.hasError('maxLength') ? 'Book sub-title max lenght is 512 characters long.' :
        this.bookCover.hasError('maxLength') ? 'Book cover URL max lenght is 1990 characters long.' :
          this.description.hasError('maxLength') ? 'Book description max lenght is 1990 characters long' : '';
  }

  onNoClick(): void {
    // this.dialogRef.close();
  }

  onSubmitClick() {

    if (this.form.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;

      this.book.userId = this.user.id;
      this.book.title = this.bookTitle.value;
      this.book.subtitle = this.bookSubTitle.value;
      this.book.coverpageUrl = this.bookCover.value;
      this.book.description = this.description.value;

     // console.log('updated book:', this.book);
      /*
      setTimeout((form: any = this) => {
        form.errorMessage = 'Airline Update Not Impllemented. ';
        form.hasSpinner = false;
      }, 300);

      */
      this.bookService.save(this.book)
        .subscribe(resBook => {
          // console.log('bookService.save', resBook);
          if (resBook && resBook.status === 'success') {

            this.toastr.success('Book Updated', 'Book Updated!');
            this.hasSpinner = false;
            this.dialogRef.close(resBook.data.book);
          } else {
            this.errorMessage = 'Book Update Failed. ' + resBook.message;
            this.hasSpinner = false;
            setTimeout((router: Router) => {
              this.errorMessage = undefined;
            }, 2000);
            // this.spinerService.display(false);
            this.toastr.error('Book Update Failed', resBook.message);
          }

        },
        err => console.error('Observer got an error: ' + err),
        () => console.log('Observer got a complete notification'));


    } else {
      this.errorMessage = 'Not valid input';
      this.toastr.error('Please enter valid values for fields', 'Not valid input');
    }
  }


}
