import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { BookModel, ChapterModel, ChapteStat } from 'src/app/@core/api/book/dto';
import { AutorModel } from 'src/app/@core/api/user/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';

const PAGE_CHARTS = 1800;
const PAGE_CHARS_HTML = '<p class="blue">In rough terms, each page of a standard-format hardcover book has about <b>300-350 words</b>, ' +
    'and each word is five characters plus a space. So a typical book page has, say <b>1,500 to 1,800 characters</b>. ' +
    'If we consider 250 pages as standard book length, then you\'re talking about maybe ' +
    '400,000 characters if you don\'t count the spaces; 500,000 if you do.</p>';

@Component({
    selector: 'app-chapter-card',
    templateUrl: './chapter-card.component.html',
    styleUrls: ['./chapter-card.component.scss']
})

export class ChapterCardComponent implements OnInit, OnDestroy {

    /**
    * BINDINGS
    */
    @Input() book: BookModel;
    @Input() autor: AutorModel;
    @Input() chapter: ChapterModel;
    @Output() chapterContent: EventEmitter<any> = new EventEmitter<any>();
    @Output() editChapter: EventEmitter<any> = new EventEmitter<any>();
    @Output() readChapter: EventEmitter<any> = new EventEmitter<any>();

    /**
     * FIELDS
     */
    user: UserModel;
    userChanged: any;
    isAdmin: boolean;
    isAutor: boolean;

    chapters: Array<ChapterModel>;
    chaptersCount: number;
    chapterText: string;
    chapterCharsCount: number;
    chapterWordsCount: number;
    chapterPagesCount: number;
    chapterStat: ChapteStat;

    constructor(private cus: CurrentUserService) {
    }


    ngOnInit(): void {
        this.chapterCharsCount = 0;
        this.chapterWordsCount = 0;
        this.chapterPagesCount = 0;
        this.chapterText = this.chapter && this.chapter.bch_content ? this.chapter.bch_content :
            (this.chapter.bch_notes ? this.chapter.bch_notes : PAGE_CHARS_HTML);
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
        this.isAutor = this.book ? this.book.userId === this.cus.user.id : false;

        if (this.chapter && this.chapter.bch_content) {
            this.chapterCharsCount = ChapterModel.CharactersCount( this.chapter.bch_content);
            this.chapterStat = ChapterModel.WordCount(this.chapter.bch_content);
            this.chapterWordsCount = this.chapterStat.wordCount;
            this.chapterPagesCount = this.chapterCharsCount / PAGE_CHARTS;
        }

        this.userChanged = this.cus.UserChanged.subscribe(newUser => {
            // console.log('newUser->', newUser);
            this.user = newUser;
            this.isAdmin = this.cus.isAdmin;
            this.isAutor = this.book ? this.book.userId ===  (this.cus.user ? this.cus.user.id : -1) : false;
        });
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    editChapterClick() {
        this.editChapter.emit(this.chapter);
    }

    chapterContentClick() {
        this.chapterContent.emit(this.chapter);
    }
}
