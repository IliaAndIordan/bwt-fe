import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { ChapterService } from 'src/app/@core/api/book/chapter.service';
import { AutorService } from 'src/app/@core/api/user/autor.service';

import { BookModel, ChapterModel } from 'src/app/@core/api/book/dto';
import { AutorModel } from 'src/app/@core/api/user/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';

import { EditChapterDialogComponent } from '../../modal/edit/edit-chapter.dialog';
import { StorageKeysLocal } from 'src/app/@core/const/bwt-storage-key.const';
import { BwtRoutes } from 'src/app/@core/const/bwt-routes.const';
import { ReadChapterDialog } from '../../modal/read/read-chapter.mobile';

@Component({
    selector: 'app-chapter-list-card',
    templateUrl: './chapter-list-card.component.html'
})

export class ChapterListCardComponent implements OnInit, OnDestroy {


    /**
    * BINDINGS
    */
    @Input() book: BookModel;
    @Input() autor: AutorModel;
    /**
     * FIELDS
     */
    user: UserModel;
    userChanged: any;
    isAdmin: boolean;
    isAutor: boolean;

    chapters: Array<ChapterModel>;
    chaptersCount: number;
    selChaptter: ChapterModel;

    /**
     * CONSTRUCTOR
     */
    constructor(
        private router: Router,
        private cus: CurrentUserService,
        private chapterService: ChapterService,
        private autorService: AutorService,
        private dialogService: MatDialog) {
    }

    ngOnInit(): void {
        this.onUserChanged();
        this.userChanged = this.cus.UserChanged.subscribe(newUser => {
            this.user = newUser;
            if (this.cus.isAuthenticated) {
                this.onUserChanged();
            } else {
                this.router.navigate(['/', BwtRoutes.Public]);
            }
        });
        this.loadChapterList();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    updateFields(): void {
        this.chaptersCount = this.chapters ? 0 : this.chapters.length;
    }

    onUserChanged(): void {
        if (this.book) {
            this.autorService.autor(this.book.userId)
                .subscribe((autorList: Array<AutorModel>) => {
                    if (autorList) {
                        this.autor = autorList[0];
                    } else {
                        this.autor = undefined;
                    }
                    this.isAutor = (this.autor && this.autor.user_id === (this.cus.user ? this.cus.user.id : -1));
                    this.isAdmin = this.cus.isAdmin;
                });
        } else {
            this.isAutor = (this.autor && this.autor.user_id === this.cus.user.id);
            this.isAdmin = this.cus.isAdmin;
        }

    }

    //#region Data

    chapterCreate() {

    }

    refreshChapterList() {
        this.chapterService.resetChapterCache();
        this.loadChapterList();
    }

    loadChapterList(): void {

        if (this.book) {

            this.chapterService.bookChapters(this.book.id)
                .subscribe((resp: Array<ChapterModel>) => {
                    this.chapters = resp;
                    // console.log('loadChapterList->', resp);
                    this.updateFields();
                });
        } else {
            this.chapters = undefined;
            this.updateFields();
        }

    }

    public editChapterDialogShow(chapter: ChapterModel): void {
        this.selChaptter = chapter;
        if (!this.selChaptter) {
            this.selChaptter = new ChapterModel();
            this.selChaptter.book_id = this.book.id;
            this.selChaptter.bch_order = this.chaptersCount + 1;
        }

        const dialogRef = this.dialogService.open(EditChapterDialogComponent, {
            data: this.selChaptter
        });

        dialogRef.afterClosed().subscribe(result => {
            // console.log('The dialog was closed', result);

            this.refreshChapterList();
        });
    }

    public readChapter(chapter: ChapterModel): void {
        this.selChaptter = chapter;
        if (this.selChaptter && this.selChaptter.bch_content &&
            this.selChaptter.bch_content.length > 0) {

            const dialogRef = this.dialogService.open(ReadChapterDialog, {
                data: this.selChaptter
            });

            dialogRef.afterClosed().subscribe(result => {
                // console.log('ReadChapterDialog was closed', result);

                this.refreshChapterList();
            });
        }
    }


    public selectChapterClick(charater: ChapterModel): void {

        this.selChaptter = charater;
        if (this.selChaptter) {

            this.cus.selectedChapter = this.selChaptter;
            // localStorage.setItem(StorageKeysLocal.SelChapter, JSON.stringify(this.selChaptter));
            this.router.navigate(['/', BwtRoutes.Chapter]);
        }

    }

    //#endregion
}
