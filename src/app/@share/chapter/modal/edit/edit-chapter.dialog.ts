import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
// Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { BookModel, ChapterModel, ResponseChapterEdit } from 'src/app/@core/api/book/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { BookService } from 'src/app/@core/api/book/book.service';
import { ChapterService } from 'src/app/@core/api/book/chapter.service';

// Constants
const Preload = 'logo-image-preload';
const Wide = 'logo-image-wide';
const Tall = 'logo-image-tall';
const NO_IMAGE = '../../../assets/images/common/noimage.png';

@Component({
    selector: 'app-edit-chapter-dialog',
    templateUrl: './edit-chapter.dialog.html',
})


export class EditChapterDialogComponent implements OnInit {

    form: FormGroup;
    hasSpinner = false;
    book: BookModel;
    user: UserModel;
    chapter: ChapterModel;

    errorMessage: string;
    imageClass: string = Preload;
    imageUrl: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private bookService: BookService,
        private chapterService: ChapterService,
        private cus: CurrentUserService,
        private toastr: ToastrService,
        public dialogRef: MatDialogRef<ChapterModel>,
        @Inject(MAT_DIALOG_DATA) public data: ChapterModel) {
        this.chapter = data;
        if (this.chapter && this.chapter.book_id) {
            this.bookService.book(this.chapter.book_id).subscribe((res: Array<BookModel>) => {
                if (res && res.length > 0) {
                    this.book = res[0];
                } else {
                    this.book = this.cus.selectedBook;
                }

            });
        } else {
            this.book = this.cus.selectedBook;
        }
    }


    ngOnInit(): void {
        this.user = this.cus.user;
        this.imageUrl = NO_IMAGE;
        this.form = this.fb.group({
            bch_id: new FormControl(this.chapter.bch_id, null),
            bch_title: new FormControl(this.chapter.bch_title, [Validators.required, Validators.minLength(5), Validators.maxLength(512)]),
            bch_subtitle: new FormControl(this.chapter.bch_subtitle, [Validators.maxLength(1000)]),
            bch_notes: new FormControl(this.chapter.bch_notes, [Validators.maxLength(1000)]),
            bch_order: new FormControl(this.chapter.bch_order, null),
        });
    }

    get bch_title() { return this.form.get('bch_title'); }
    get bch_subtitle() { return this.form.get('bch_subtitle'); }
    get bch_notes() { return this.form.get('bch_notes'); }
    get bch_order() { return this.form.get('bch_order'); }

    get errorName(): string {
        return this.bch_title.hasError('required') ? 'Chapter title is required.' :
            this.bch_title.hasError('minlength') ? 'Chapter title must be et least 5 characters long' : '';
    }

    get errorMaxLenght(): string {
        return this.bch_title.hasError('maxLength') ? 'Chapter title max lenght is 512 characters long.' :
            this.bch_subtitle.hasError('maxLength') ? 'Chapter subtitle max lenght is 1000 characters long.' :
                this.bch_notes.hasError('maxLength') ? 'Character notes max lenght is 2000 characters long.' : '';
    }

    onNoClick(): void {
        // this.dialogRef.close();
    }

    onSubmitClick() {

        if (this.form.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            this.chapter.bch_title = this.bch_title.value;
            this.chapter.bch_subtitle = this.bch_subtitle.value;
            this.chapter.bch_notes = this.bch_notes.value;
            this.chapter.bch_order = this.bch_order.value;

            this.chapter.book_id = this.book.id;

            // 'updated chapter:', this.chapter);
            /*
            setTimeout((form: any = this) => {
              form.errorMessage = 'Airline Update Not Impllemented. ';
              form.hasSpinner = false;
            }, 300);
            */
            this.chapterService.save(this.chapter)
              .subscribe((res: Array<ChapterModel>) => {
                // console.log('bookService.save', resBook);
                if (res) {

                  this.toastr.success('Chapter Updated', 'Chapter Updated!');
                  this.hasSpinner = false;
                  this.dialogRef.close(res[0]);
                } else {
                  // this.errorMessage = 'Chapter Update Failed. ' + res.message;
                  this.hasSpinner = false;
                  setTimeout((router: Router) => {
                    this.errorMessage = undefined;
                  }, 2000);
                  // this.spinerService.display(false);
                  // this.toastr.error('Chapter Update Failed', res.message);
                }

              });

        } else {
            this.errorMessage = 'Not valid input';
            this.toastr.error('Please enter valid values for fields', 'Not valid input');
        }
    }

    onImageLoad() {
        this.imageClass = this.getImageClass();
    }

    onImageError() {
        this.imageUrl = NO_IMAGE; // Don't show broken image.
    }

    getImageClass() {
        const image = new Image();
        image.src = this.imageUrl;
        if (image.width > image.height + 20 || image.width === image.height) {
            // return wide image class
            return Wide;
        } else {
            return Tall;
        }
    }

}
