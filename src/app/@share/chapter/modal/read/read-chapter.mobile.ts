import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
// Services
import { ChapterService } from 'src/app/@core/api/book/chapter.service';
// Models
import { ChapterModel, BookModel, ChaptePage, ChapteStat } from 'src/app/@core/api/book/dto';
import { BookService } from 'src/app/@core/api/book/book.service';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';

const NO_IMAGE = '../../../assets/images/common/noimage.png';

@Component({
    selector: 'app-read-chapter-dialog',
    templateUrl: './read-chapter.mobile.html',
    styleUrls: ['./read-chapter.mobile.scss']
})


// tslint:disable-next-line:component-class-suffix
export class ReadChapterDialog implements OnInit {

    chapter: ChapterModel;
    book: BookModel;
    imageUrl: string;
    pages: ChaptePage[];
    stat: ChapteStat;

    constructor(
        private cus: CurrentUserService,
        private chapterService: ChapterService,
        private toastr: ToastrService,
        public dialogRef: MatDialogRef<ChapterModel>,
        @Inject(MAT_DIALOG_DATA) public data: ChapterModel) {
        this.chapter = data;
    }


    ngOnInit(): void {
        this.book = this.cus.selectedBook;
        const text = ChapterModel.StripHtml(this.chapter.bch_content);
        this.stat = ChapterModel.WordCount(this.chapter.bch_content);
        this.pages = ChapterModel.getPages(this.chapter.bch_content, 9000);
        console.log('ngOnInit -> pages: ',this.pages);
        if (this.book && this.book.coverpageUrl) {
            this.imageUrl = this.book.coverpageUrl;
        } else {
            this.imageUrl = NO_IMAGE;
        }
        this.getImage();
    }

    getImage() {
        const image = new Image();
        image.src = this.imageUrl;
    }


    onImageLoad() {
        this.getImage();
    }

    onImageError() {
        this.imageUrl = NO_IMAGE;
    }

    close() {
        this.dialogRef.close(false);
    }
}

