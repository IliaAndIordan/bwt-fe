import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { BookModel, ChapterModel, ChapteStat } from 'src/app/@core/api/book/dto';
import { AutorModel } from 'src/app/@core/api/user/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';

const PAGE_CHARTS = 1800;
const PAGE_CHARS_HTML = '<p class="blue">In rough terms, each page of a standard-format hardcover book has about <b>300-350 words</b>, ' +
    'and each word is five characters plus a space. So a typical book page has, say <b>1,500 to 1,800 characters</b>. ' +
    'If we consider 250 pages as standard book length, then you\'re talking about maybe ' +
    '400,000 characters if you don\'t count the spaces; 500,000 if you do.</p>';

@Component({
    selector: 'app-chapter-card',
    templateUrl: './chapter-stat-card.component.html',
})

export class ChapterStatCardComponent implements OnInit, OnDestroy {

    /**
    * BINDINGS
    */
    @Input() chapterStat: ChapteStat;
    @Input() chapter: ChapterModel;

    /**
     * FIELDS
     */
    isAutor: boolean;

    constructor(
        private cus: CurrentUserService) {}


    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        // if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

}
