import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChapterListCardComponent } from './lists/card/chapter-list-card.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BwtMaterialModule } from '../material/material.module';
import { ModalModule } from '../modal/modal.module';
import { ChapterCardComponent } from './card/chapter-card.component';
import { EditChapterDialogComponent } from './modal/edit/edit-chapter.dialog';
import { ReadChapterDialog } from './modal/read/read-chapter.mobile';

@NgModule({

    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        // Custom Modules
        BwtMaterialModule,
        ModalModule,
    ],
    declarations: [
        ChapterListCardComponent,
        ChapterCardComponent,
        EditChapterDialogComponent,
        ReadChapterDialog,
    ],
    exports: [
        ChapterListCardComponent,
        ChapterCardComponent,
    ],
    entryComponents: [
        EditChapterDialogComponent,
        ReadChapterDialog,
    ]
})

export class ChapterModule {

    constructor() { }
}
