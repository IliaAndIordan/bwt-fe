import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BookModel, CharacterModel } from 'src/app/@core/api/book/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AutorModel } from 'src/app/@core/api/user/dto';
const Preload = 'lv-img-sm';
const Wide = 'lv-img-sm';
const Tall = 'lv-img-sm';

@Component({
  selector: 'app-crtr-list-small-card',
  templateUrl: './crtr-list-small-card.component.html',
})
export class CharacterListSmallCardComponent implements OnInit {

  isAutor: boolean;
  /**
  * CONSTRUCTOR
  */
  constructor(private cus: CurrentUserService) { }

  /**
   * BINDINGS
   */
  @Input() book: BookModel;
  @Input() autor: AutorModel;
  @Input() character: CharacterModel;
  @Input() cover: string;
  @Output() cardClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() editClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectClicked: EventEmitter<any> = new EventEmitter<any>();

  /**
   * FIELDS
   */
  imageClass: string = Preload;

  ngOnInit() {
    this.isAutor = (this.autor && this.autor.user_id ===  (this.cus.user ? this.cus.user.id : -1));
  }

  onImageLoad() {
    this.imageClass = this.getImageClass();
  }

  onImageError() {
    this.cover = '../../../assets/images/common/zac_efron_naked.jpg'; // Don't show broken image.
  }

  getImageClass() {
    const image = new Image();
    image.src = this.cover;
    if (image.width > image.height + 20 || image.width === image.height) {
      // return wide image class
      return Wide;
    } else {
      return Tall;
    }
  }

  closeCard() {

  }

  onClick() {
    this.cardClicked.emit(this.book.id);
  }

  onEditClicked() {
    this.editClicked.emit(this.character);
  }

  onSelectClicked() {
    this.selectClicked.emit(this.character);
  }
}

