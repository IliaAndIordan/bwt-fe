import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { BookModel, CharacterModel } from 'src/app/@core/api/book/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AutorModel } from 'src/app/@core/api/user/dto';
import { CharacterService } from 'src/app/@core/api/book/character.service';
import { MatDialog } from '@angular/material/dialog';
import { EditCharacterDialogComponent } from '../../modal/edit-character/edit-character.dialog';
import { AutorService } from 'src/app/@core/api/user/autor.service';

const Preload = 'simple-logo-card-image-preload';
const Wide = 'simple-logo-card-image-wide';
const Tall = 'simple-logo-card-image-tall';

@Component({
    selector: 'app-ctrt-list-small',
    templateUrl: './character-list-small.component.html'
})

export class CharacterListSmallComponent implements OnInit, OnDestroy {

    /**
        * BINDINGS
        */
    @Input() book: BookModel;
    @Input() autor: AutorModel;

    /**
     * FIELDS
     */
    user: UserModel;
    userChanged: any;
    isAdmin: boolean;
    isAutor: boolean;

    characters: Array<CharacterModel>;
    crtrCount: number;

    imageClass: string = Preload;
    /**
    * CONSTRUCTOR
    */
    constructor(private cus: CurrentUserService,
        private crtrService: CharacterService,
        private autorService: AutorService,
        private dialogService: MatDialog) {
    }



    ngOnInit() {

        this.onUserChanged();
        this.userChanged = this.cus.UserChanged.subscribe(newUser => {
            // console.log('newUser->', newUser);
            this.user = newUser;
            this.onUserChanged();
        });
        this.loadCharacterList();
    }


    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    onUserChanged(): void {

        if (this.book && this.cus.isAuthenticated) {
            this.autorService.autor(this.book.userId)
                .subscribe((autorList: Array<AutorModel>) => {
                    if (autorList) {
                        this.autor = autorList[0];
                    } else {
                        this.autor = undefined;
                    }
                    this.isAutor = (this.autor && this.autor.user_id === (this.cus.user ? this.cus.user.id : -1));
                    this.isAdmin = this.cus.isAdmin;
                });
        } else {
            this.isAutor = (this.autor && this.autor.user_id === (this.cus.user ? this.cus.user.id : -1));
            this.isAdmin = this.cus.isAdmin;
        }

    }

    updateFields(): void {
        this.crtrCount = this.characters ? 0 : this.characters.length;
    }

    onRefreshListClicked() {
        this.crtrService.resetCharacterCache();
        this.loadCharacterList();
    }

    //#region Data

    loadCharacterList(): void {

        if (this.book) {

            this.crtrService.bookCharacters(this.book.id)
                .subscribe((resp: Array<CharacterModel>) => {
                    this.characters = resp;
                    this.crtrCount = this.characters ? this.characters.length : 0;
                    // console.log('loadCharacterList->', resp);

                });
        } else {
            this.characters = undefined;
            this.updateFields();
        }

    }

    public editCharacterDialogShow(crtr: CharacterModel): void {

        if (!crtr) {
            crtr = new CharacterModel();
            crtr.book_id = this.book.id;
            crtr.crtr_order = this.crtrCount + 1;
        }

        const dialogRef = this.dialogService.open(EditCharacterDialogComponent, {
            data: crtr
        });

        dialogRef.afterClosed().subscribe(result => {
            // console.log('The dialog was closed', result);

            this.onRefreshListClicked();
        });
    }

    //#endregion

}
