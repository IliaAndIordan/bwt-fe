import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'truncate'
})
export class TruncatePipe implements PipeTransform {
    transform(value: string, args: number): string {

        if (args === 0) {
            return value;
        }
        // Set limit to 10 if none declared
        let limit = Number(args) > 3 ? (args - 3) : 10;
        // Trail string
        const trail = '...';

        if (value) {
            let isUnicode = false;
            for (let idx = 0; idx < value.length; idx++) {
                if (value.charCodeAt(idx) > 127) {
                    isUnicode = true;
                    break;
                }
            }

            limit = isUnicode ? limit * 2 : limit;
            const div = document.createElement('div');
            div.innerHTML = value;
            const text = div.textContent;
            if (text.length > limit) {
                return text.substring(0, limit) + trail;
            } else {
                return text;
            }

        }

    }
}
