import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BookTypeNameDisplayPipe } from 'src/app/@core/api/book/enums';
import { UserRoleDisplayPipe } from 'src/app/@core/auth/api/enums';
import { TruncatePipe } from './truncate.pipe';



@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    TruncatePipe,
    UserRoleDisplayPipe,
    BookTypeNameDisplayPipe,
  ],
  exports: [
    TruncatePipe,
    UserRoleDisplayPipe,
    BookTypeNameDisplayPipe,
  ],

})
export class PipelModule { }
