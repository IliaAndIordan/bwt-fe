import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';  // <-- #1 import module

import { BwtMaterialModule } from './material/material.module';
import { ToastrModule } from 'ngx-toastr';
// import { ChartsModule } from 'ng2-charts';
import { ModalModule } from './modal/modal.module';
import { BookListComponent } from './book-list/book-list.component';
import { BookListCardComponent } from './book-list/card/book-list-card.component';
import { MatTabGroup } from '@angular/material/tabs';
import { BookCardComponent } from './book/card/book-card.component';
import { CharacterListSmallComponent } from './character/list-small/character-list-small.component';
import { BwtCoreModule } from '../@core/core.module';
import { CharacterListSmallCardComponent } from './character/cards/list-small/crtr-list-small-card.component';
import { ChapterModule } from './chapter/chapter.module';
import { ChapterListCardComponent } from './chapter/lists/card/chapter-list-card.component';
// import { CardsModule } from './cards/cards.module';


@NgModule({

    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        // Custom Modules
        BwtMaterialModule,
        ModalModule,
        ChapterModule,
    ],
    declarations: [
        BookListComponent,
        BookListCardComponent,
        BookCardComponent,
        CharacterListSmallComponent,
        CharacterListSmallCardComponent,
    ],
    exports: [
        BwtMaterialModule,
        ModalModule,
        BookListComponent,
        BookListCardComponent,
        BookCardComponent,
        CharacterListSmallComponent,
        CharacterListSmallCardComponent,
        ChapterListCardComponent,
    ],
    entryComponents: [
    ]
})

export class SharedModule {

    constructor() { }
}

