import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
// Constants
// Services
import { UserModel } from 'src/app/@core/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { AuthService } from 'src/app/@core/auth/api/auth.service';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { BookModel } from 'src/app/@core/api/book/dto';
import { BwtRoutes } from 'src/app/@core/const/bwt-routes.const';
import { BookService } from 'src/app/@core/api/book/book.service';
import { MatDialog } from '@angular/material/dialog';
import { EditBookDialogComponent } from '../modal/edit-book/edit-book.dialog';
import { StorageKeysLocal } from 'src/app/@core/const/bwt-storage-key.const';

@Component({
    selector: 'app-book-list',
    templateUrl: './book-list.component.html',
})

export class BookListComponent implements OnInit, OnDestroy {

    @Input() books: Array<BookModel>;
    @Output() refreshListClick: EventEmitter<any> = new EventEmitter<any>();

    user: UserModel;
    userChanged: any;
    isAdmin: boolean;
    selBook: BookModel;

    colspan4Card = 3;


    constructor(
        private router: Router,
        private breakpointObserver: BreakpointObserver,
        private cus: CurrentUserService,
        private authService: AuthService,
        private bookService: BookService,
        private dialogService: MatDialog) { }



    ngOnInit(): void {
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
        this.breakpointObserverInit();
        this.userChanged = this.cus.UserChanged.subscribe(newUser => {
            // console.log('newUser->', newUser);
            this.user = newUser;
            this.isAdmin = this.cus.isAdmin;
        });
    }

    breakpointObserverInit(): void {
        this.breakpointObserver
            .observe([Breakpoints.Handset])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    // console.log('Matches Handset viewport  mode');
                    this.colspan4Card = 6;
                }
            });
        this.breakpointObserver
            .observe([Breakpoints.Tablet])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    // console.log('Matches Tablet viewport mode');
                    this.colspan4Card = 4;
                }
            });

        this.breakpointObserver
            .observe([Breakpoints.Web])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    // console.log('Matches Web viewport mode');
                    this.colspan4Card = 3;
                }
            });
        this.breakpointObserver
            .observe([Breakpoints.Small])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    // console.log('Matches Handset viewport  mode');
                    this.colspan4Card = 6;
                }
            });

    }


    ngOnDestroy(): void {
        if (this.userChanged) {
            this.userChanged.unsubscribe();
        }
    }

    onRefreshListClick() {
        this.refreshListClick.emit();
    }

    //#region Navigation

    onBookCreate() {
        if (this.cus.user && this.cus.isAuthenticated) {
            let newBook = new BookModel();
            newBook.title = 'New Book';
            this.editBookDialogShow(newBook);
        } else {
            this.router.navigate(['/', BwtRoutes.Public]);
        }

    }

    //#endregion

    //#region DATA

    onBookClick(bookId: number): void {
        // console.log('onBookClick-> bookId=', bookId);
    }


    public editBookDialogShow(book: BookModel): void {
        this.selBook = book;
        if (!this.selBook) {
            this.selBook = new BookModel();
            this.selBook.title = 'New Book';
        }
        const dialogRef = this.dialogService.open(EditBookDialogComponent, {
            data: this.selBook
        });

        dialogRef.afterClosed().subscribe(result => {
            // console.log('The dialog was closed', result);
            if (result) {
                this.onRefreshListClick();
            }
        });
    }


    public selectBookclicked(book: BookModel): void {
        this.selBook = book;
        if (book) {
            // 'selectBookclicked', book);
            localStorage.setItem(StorageKeysLocal.SelBook, JSON.stringify(book));
            this.router.navigate(['/', BwtRoutes.Book]);
        }

    }
    //#endregion


}
