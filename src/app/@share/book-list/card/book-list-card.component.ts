import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BookModel } from 'src/app/@core/api/book/dto';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
const Preload = 'simple-logo-card-image-preload';
const Wide = 'simple-logo-card-image-wide';
const Tall = 'simple-logo-card-image-tall';

@Component({
  selector: 'app-book-list-card',
  templateUrl: './book-list-card.component.html',
  styleUrls: ['book-list-card.component.scss']
})
export class BookListCardComponent implements OnInit {

  isAutor: boolean;
  /**
  * CONSTRUCTOR
  */
  constructor(private cus: CurrentUserService) { }

  /**
   * BINDINGS
   */
  @Input() book: BookModel;
  @Input() autor: UserModel;
  @Input() cover: string;
  @Output() cardClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() editBookClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectBookClicked: EventEmitter<any> = new EventEmitter<any>();

  /**
   * FIELDS
   */
  imageClass: string = Preload;

  ngOnInit() {
    this.isAutor = this.autor.id === this.cus.user.id;
  }

  onImageLoad() {
    this.imageClass = this.getImageClass();
  }

  onImageError() {
    this.cover = '../../../assets/images/common/man-writing_04.jpg'; // Don't show broken image.
  }

  getImageClass() {
    const image = new Image();
    image.src = this.cover;
    if (image.width > image.height + 20 || image.width === image.height) {
      // return wide image class
      return Wide;
    } else {
      return Tall;
    }
  }

  closeCard() {

  }

  onClick() {
    this.cardClicked.emit(this.book.id);
  }

  onEditBookClicked() {
    this.editBookClicked.emit(this.book);
  }

  onSelectBookClicked() {
    this.selectBookClicked.emit(this.book);
  }
}

