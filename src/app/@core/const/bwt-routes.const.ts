export const BwtRoutes = {
    Root: '',
    Public: 'public',
    Profile: 'profile',
    Login: 'login',
    Dashboard: 'dashboard',
    Book: 'book',
    NotAuthorized: 'not-authorized',
    UserProfile: 'user-profile',
    SelectedChapter: 'book-chapter',
    Chapter: 'chapter',
};
