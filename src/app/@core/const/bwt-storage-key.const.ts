export const StorageKeysLocal = {
    Books: 'bwt_local_books',
    BookAutors: 'bwt_local_book_autors',
    Characters: 'bwt_local_characters',
    Chapters: 'bwt_local_chapters',
    SelBook: 'bwt_local_book_selected',
    SelCharacter: 'bwt_local_character_selected',
    SelChapter: 'bwt_local_chapter_selected',
};

export const StorageKeysSession = {
    BearerToken: 'jwt_token_key',
    RefreshToken: 'jwt_refresh_token_key',
    CurrentUser: 'bwt_local_user_key',
    LocalUserSettings: 'bwt_local_user_settings',
};

// export const AVATAR_IMAGE_URL = './../../../../../assets/images/common/iziordanov_sd_128_bw.png';

export const BASE_IMAGE_URL = 'https://common.iordanov.info/images/';
export const AMS_IMAGE_URL = BASE_IMAGE_URL + 'ams/';
export const COMMON_IMAGE_URL = AMS_IMAGE_URL + 'common/';
export const AVATAR_IMAGE_URL = COMMON_IMAGE_URL + 'iziordanov_sd_128_bw.png';
export const NO_IMAGE_URL = COMMON_IMAGE_URL + 'noimage.png';
export const NO_COVER_URL = COMMON_IMAGE_URL + 'man-writing_03.jpg';


