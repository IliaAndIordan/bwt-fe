
export const HelpMainCharacterCreate = {
    title: 'Create an unforgettable main character.',
    /*
    text: [
        'Before you start page one, you’ll want to understand your characters.',
        'Your most important character will be your protagonist, also known as your lead or your hero/heroine. ' +
        'This main character must have a character arc, in other words be a different, better person by the end. ' +
        'That means he (and I’m using this pronoun inclusively to mean hero or heroine) ' +
        'must have potentially heroic qualities that emerge in the climax. ' +
        'Your lead can have human flaws, but those should be redeemable.',
        'You’ll also have an antagonist, the villain who should be every bit as formidable and compelling ' +
        'as your hero. Make sure the bad guy isn’t bad just because he’s the bad guy. ' +
        'You must have reasons for why he does what he does to make him a worthy foe, ' +
        'realistic and memorable. You’ll also need important orbital cast members.'
    ]*/
};
