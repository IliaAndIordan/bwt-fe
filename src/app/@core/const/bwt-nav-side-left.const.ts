import { UserRole } from '../auth/api/enums';
import { BwtRoutes } from './bwt-routes.const';

export const BwtNavSideLeft = [
    {
        title: 'Home',
        url: '/' + BwtRoutes.Public,
        icon: 'home',
        authorised: false,
        userRole: UserRole.User
    },
    {
        title: 'Dashboard',
        url: '/' + BwtRoutes.Dashboard,
        icon: 'dashboard',
        authorised: true,
        userRole: UserRole.User
    },
];
