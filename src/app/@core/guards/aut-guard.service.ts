import { Injectable } from '@angular/core';
import { Router, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanLoad } from '@angular/router';
import { CurrentUserService } from '../auth/current-user.service';
import { TokenService } from '../auth/token.service';
import { BwtRoutes } from '../const/bwt-routes.const';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(private router: Router,
    private cus: CurrentUserService,
    private tokenService: TokenService) { }

  canLoad(route: Route) {
    return true;
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.cus.isAuthenticated && this.cus.user) {
      return true;
    }

    this.router.navigate(['/', BwtRoutes.Public], { queryParams: { redirectTo: state.url } });
    return false;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }
}
