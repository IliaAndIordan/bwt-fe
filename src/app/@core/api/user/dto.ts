import { ResponseModel } from '../responce.model';

export class AutorModel {
    public user_id: number;
    public e_mail: string;
    public is_receive_emails: boolean;
    public user_name: string;

    public static fromJSON(json: IAutorModel): AutorModel {
        const vs = Object.create(AutorModel.prototype);
        return Object.assign(vs, json, {
            is_receive_emails: (json && json.is_receive_emails) === 1 ? true : false,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? AutorModel.fromJSON(value) : value;
    }

    public toJSON(): IAutorModel {

        return Object.assign({}, this, {
            is_receive_emails: this.is_receive_emails ? 1 : 0,
        });

    }
}

export interface IAutorModel {
    user_id: number;
    e_mail: string;
    is_receive_emails: number;
    user_name: string;
}


export class BookAutor {
    book_autor: Array<IAutorModel>;
}

export interface ResponseBookAutor extends ResponseModel {
    data: BookAutor;
}
