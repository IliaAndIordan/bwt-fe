import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
// Custom
import { ApiHelper } from '../api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
import { ServiceNames } from '../api.constants';
import { BaseService } from '../api.service';
import { StorageKeysLocal } from '../../const/bwt-storage-key.const';
import { AutorModel, IAutorModel, ResponseBookAutor } from './dto';


@Injectable()
export class AutorService extends BaseService {

    protected observable: Observable<any>;

    constructor(private apiHelper: ApiHelper,
        private http: HttpClient,
        private cus: CurrentUserService) {
        super();
    }

    //#region Data

    private get autors(): Array<AutorModel> {
        const items = new Array<AutorModel>();
        const data = localStorage.getItem(StorageKeysLocal.BookAutors);
        if (data) {
            const parsedData: Array<IAutorModel> = JSON.parse(data);
            parsedData.forEach((element: IAutorModel) => {
                const tmp: AutorModel = AutorModel.fromJSON(element);
                items.push(tmp);
            });
        }
        return items;
    }

    public resetAutorsCache(): void {
        localStorage.removeItem(StorageKeysLocal.BookAutors);
    }

    private updateAutorsCashe(data: Array<AutorModel>): void {

        if (data) {

            // console.log('updateRwCashe-> data.length=', data.length);
            const items = this.autors;
            // console.log('updateRwCashe-> data', data);

            data.forEach((element: AutorModel) => {

                // console.log('updateRwCashe-> element', element);

                const toUpdate = items.find(x => x.user_id === element.user_id);
                // console.log('updateRwCashe-> toUpdate', toUpdate);
                if (toUpdate) {
                    const idx = items.indexOf(toUpdate);
                    items[idx] = element;
                } else {
                    items.push(element);
                }
            });
            // console.log('updateRwCashe-> runways', rw);
            localStorage.setItem(StorageKeysLocal.BookAutors, JSON.stringify(items));

        }
    }

    private autorFromCache(id: number): Array<AutorModel> {
        let data = new Array<AutorModel>();
        if (id) {
            const cacheData = this.autors;
            if (cacheData && cacheData.length > 0) {
                data = cacheData.filter(x => x.user_id === id);
            }
        }

        return data;
    }

    public autor(id: number): Observable<Array<AutorModel>> {

        let data = new Array<AutorModel>();
        if (id) {
            data = this.autorFromCache(id);
            if (!data || data.length === 0) {

                this.observable = this.getAutor(id).pipe(
                    map((responce: ResponseBookAutor) => {
                        this.observable = undefined;
                        data = new Array<AutorModel>();
                        if (responce.data.book_autor) {
                            responce.data.book_autor.forEach((element: IAutorModel) => {
                                const tmp: AutorModel = AutorModel.fromJSON(element);
                                data.push(tmp);
                            });
                            this.updateAutorsCashe(data);
                            data = this.autorFromCache(id);
                        }

                        return data;
                    }),
                    tap(res => { this.log(`getAutor`); }),
                    catchError(this.handleError())
                );

            } else {
                // console.log('data already available');
                return of(data);
            }
        }

        return this.observable;
    }

    //#endregion

    //#region Runway Http Calls

    public getAutor(userId: number): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/book_autor',
            { 'user_id': userId }, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getBookList`); }),
                catchError(this.handleError('getBookList', []))
            );
    }

    //#endregion

    //#region URL Methods

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.base);
    }

    //#endregion
}
