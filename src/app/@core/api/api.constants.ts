export const ServiceUrl = [
    {
        name: 'base',
        url: 'https://ws.bwt.iordanov.info'
    },
    {
        name: 'autenticate',
        url: 'https://ws.bwt.iordanov.info/login'
    },
    {
        name: 'refreshToken',
        url: 'https://ws.vlb.iordanov.info/jwtrefresh'
    },
    {
        name: 'book',
        url: 'https://ws.bwt.iordanov.info/book'
    },
];

export const ServiceNames = {
    base: 'base',
    autenticate: 'autenticate',
    refreshToken: 'refreshToken',
    book: 'book',
};
