import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
// Constants
import { ServiceNames, ServiceUrl } from './api.constants';
// Service
import { TokenService } from '../auth/token.service';
import { AuthService } from '../auth/api/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';




@Injectable()
export class ApiHelper {

    public ServiceNames = ServiceNames;
    public ServiceUrl = ServiceUrl;
    protected observable: Observable<any>;

    constructor(private tokenService: TokenService,
        private authService: AuthService,
        private toastr: ToastrService) { }

    /**
     * @method getServiceUrl
     * @param serviceName
     * @returns {string}
     */
    public getServiceUrl(serviceName: string): string {
        let retValue = null;
        const sn = ServiceNames[serviceName];
        const urlFilter = ServiceUrl.filter(x => x.name === serviceName);
        // console.log('urlFilter.length:' + urlFilter.length);
        if (urlFilter && urlFilter.length > 0) {
            retValue = urlFilter[0].url;
        }

        // console.log('sn: ' + sn + ' for serviceUrl:', retValue);
        return retValue;
    }

    private refreshToken(): void {
        if (this.tokenService.barerToken && this.tokenService.isTokenExpired()) {
            console.log('try to refresh token...');
            this.authService.refreshToken()
                .subscribe(res => {
                    console.dir(res);
                    this.toastr.info('Tocken Refreshed', 'Api Helper');
                });
        }
    }


}
