import { Pipe, PipeTransform } from '@angular/core';
import { EnumViewModel } from '../../models/common/enum-view.model';

export enum BootType {
    Poetry = 1,
    Prose = 2,
    Drama = 3,
    NonFiction = 4,
    Media = 5,
}

export const BootTypeOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Poetry'
    },
    {
        id: 2,
        name: 'Prose'
    },
    {
        id: 3,
        name: 'Drama'
    },
    {
        id: 4,
        name: 'Non-Fiction'
    },
    {
        id: 5,
        name: 'Media'
    },
];


@Pipe({ name: 'booktypename' })
export class BookTypeNameDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = BootType[value];
        const data: EnumViewModel = BootTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}
