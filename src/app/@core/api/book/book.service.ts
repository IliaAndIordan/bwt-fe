import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
// Custom
import { ApiHelper } from '../api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
import { ServiceNames } from '../api.constants';
import { BaseService } from '../api.service';
import { StorageKeysLocal } from '../../const/bwt-storage-key.const';
import { BookModel, IBookModel, ResponseBookList, ResponseBookEdit } from './dto';


@Injectable()
export class BookService extends BaseService {

    protected observable: Observable<any>;

    constructor(private apiHelper: ApiHelper,
        private http: HttpClient,
        private cus: CurrentUserService) {
        super();
    }

    //#region Data

    private get books(): Array<BookModel> {
        const items = new Array<BookModel>();
        const data = localStorage.getItem(StorageKeysLocal.Books);
        if (data) {
            const parsedData: Array<IBookModel> = JSON.parse(data);
            parsedData.forEach((element: IBookModel) => {
                const tmp: BookModel = BookModel.fromJSON(element);
                items.push(tmp);
            });
        }
        return items;
    }

    public resetBookCache(): void {
        localStorage.removeItem(StorageKeysLocal.Books);
    }

    private updateBookCashe(data: Array<BookModel>): void {

        if (data) {

            // console.log('updateRwCashe-> data.length=', data.length);
            const items = this.books;
            // console.log('updateRwCashe-> data', data);

            data.forEach((element: BookModel) => {

                // console.log('updateRwCashe-> element', element);

                const bookIdx = items.findIndex(x => x.id === element.id);
                // console.log('updateRwCashe-> toUpdate', toUpdate);
                if (bookIdx > -1) {
                    items[bookIdx] = element;
                } else {
                    items.push(element);
                }
            });
            // console.log('updateRwCashe-> runways', rw);
            localStorage.setItem(StorageKeysLocal.Books, JSON.stringify(items));

        }
    }

    private bookFromCache(id: number): Array<BookModel> {
        let data = new Array<BookModel>();
        if (id) {
            const cacheData = this.books;
            if (cacheData && cacheData.length > 0) {
                data = cacheData.filter(x => x.id === id);
            }
        }

        return data;
    }

    public book(id: number): Observable<Array<BookModel>> {

        let data = new Array<BookModel>();
        if (id) {
            data = this.bookFromCache(id);
            if (!data || data.length === 0) {

                this.observable = this.getBookList().pipe(
                    map((responce: ResponseBookList) => {
                        this.observable = undefined;
                        data = new Array<BookModel>();
                        if (responce && responce.data && responce.data.book_list) {
                            responce.data.book_list.forEach((element: IBookModel) => {
                                const tmp: BookModel = BookModel.fromJSON(element);
                                data.push(tmp);
                            });
                            this.updateBookCashe(data);
                            data = this.bookFromCache(id);
                        }

                        return data;
                    }),
                    tap(res => { this.log(`bookFromCache`); }),
                    catchError(this.handleError())
                );

            } else {
                // console.log('data already available');
                return of(data);
            }
        }

        return this.observable;
    }

    public bookList(): Observable<Array<BookModel>> {

        let data = this.books;
        if (!data || data.length === 0) {

            this.observable = this.getBookList().pipe(
                map((responce: ResponseBookList) => {
                    this.observable = undefined;
                    data = new Array<BookModel>();
                    if (responce && responce.data && responce.data.book_list) {
                        responce.data.book_list.forEach((element: IBookModel) => {
                            const tmp: BookModel = BookModel.fromJSON(element);
                            data.push(tmp);
                        });
                        this.updateBookCashe(data);
                        return data;
                    }
                }),
                tap(res => { this.log(`getBookList`); }),
                catchError(this.handleError())
            );

        } else {
            // console.log('data already available');
            return of(data);
        }

        return this.observable;
    }

    //#endregion

    //#region Runway Http Calls

    public getBookList(): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/book_list',
            { 'user_id': (this.cus.user ? this.cus.user.id : -1) }, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getBookList`); }),
                catchError(this.handleError('getBookList', []))
            );
    }

    public save(myBook: BookModel): Observable<ResponseBookEdit> {
        if (myBook.userId === this.cus.user.id ||
            (myBook.userId !== this.cus.user.id && this.cus.isAdmin)) {

                myBook.userId = this.cus.user.id;
            const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
            return this.http.post(this.sertviceUrl + '/book_save',
                {book: myBook},
                { headers: hdrs }).pipe(
                    map((res: ResponseBookEdit) => res),
                    tap(event => {
                        this.log(`tap Book save event: ` +  JSON.stringify(event));
                        // There may be other events besides the response.
                        if (event instanceof HttpResponse) {
                          // cache.put(req, event); // Update the cache.
                        }
                      }),
                );
        }
    }

    //#endregion

    //#region URL Methods

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.book);
    }

    //#endregion
}
