import { ResponseModel } from '../responce.model';
import { DateModel, IDateModel } from '../../auth/date.model';
import { NO_COVER_URL } from '../../const/bwt-storage-key.const';
import { BootType } from './enums';

//#region BookModel

export class BookModel {

    public id: number;
    public title: string;
    public subtitle: string;
    public coverpageUrl: string;
    public typeId: BootType;
    public description: string;
    public adate: Date;
    public udate: Date;
    public userId: number;

    public static fromJSON(json: IBookModel): BookModel {
        const vs = Object.create(BookModel.prototype);
        return Object.assign(vs, json, {
            coverpageUrl: json.coverpageUrl ? json.coverpageUrl : NO_COVER_URL,
            typeId: json.typeId ? json.typeId as BootType : BootType.Poetry,
            adate: (json && json.adate) ? new Date(Date.parse(json.adate)) : undefined,
            udate: (json && json.udate) ? new Date(Date.parse(json.udate)) : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? BookModel.fromJSON(value) : value;
    }
    /*
        public toJSON(): IBookModel {
    
            return Object.assign({}, this, {
                adate: this.adate ? (this.adate instanceof Date ?
                    this.adate.toJSON() : undefined) : undefined,
                udate: this.udate ?  (this.udate instanceof Date ?
                    this.udate.toJSON() : undefined) : undefined,
            });
    
        }
        */
}

export interface IBookModel {
    id: number;
    title: string;
    subtitle: string;
    coverpageUrl: string;
    typeId: number;
    description: string;
    adate: string;
    udate: string;
    userId: number;
}


export class BookList {
    book_list: Array<IBookModel>;
}

export interface ResponseBookList extends ResponseModel {
    data: BookList;
}

export class BookEdit {
    book: BookModel;
}

export interface ResponseBookEdit extends ResponseModel {
    data: BookEdit;
}

//#endregion

//#region CharacterModel

export class CharacterModel {

    public crtr_id: number;
    public crtr_name: string;
    public crtr_nikname: string;
    public crtr_url: string;
    public book_id: number;
    public crtr_desc: string;
    public crtr_order: number;
    public crtr_image_url: string;


}



export class CharacterList {
    book_chrtr_list: Array<CharacterModel>;
}

export interface ResponseCharacterList extends ResponseModel {
    data: CharacterList;
}

export class CharacterEdit {
    character: CharacterModel;
}

export interface ResponseCharacterEdit extends ResponseModel {
    data: CharacterEdit;
}


//#endregion

//#region  Chapter Models

export class ChapteStat {
    public shortWordCount: number;
    public longWordCount: number;
    public singleLetterWordCount: number;
    public wordCount: number;
    public charsCount: number;
}

export class ChaptePage {
    public pageNr: number;
    public content: string;
    public startWord: number;
    public endWord: number;
    public charsCount: number;
}

export class ChapterModel {

    public bch_id: number;
    public bch_title: string;
    public bch_subtitle: string;
    public bch_notes: string;
    public book_id: number;
    public bch_content: string;
    public bch_order: number;
    public adate: Date;
    public udate: Date;


    public static fromJSON(json: IChapterModel): ChapterModel {
        const vs = Object.create(ChapterModel.prototype);
        return Object.assign(vs, json, {
            adate: (json && json.adate) ? new Date(Date.parse(json.adate)) : undefined,
            udate: (json && json.udate) ? new Date(Date.parse(json.udate)) : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? ChapterModel.fromJSON(value) : value;
    }
    /*
        public toJSON(): IChapterModel {
    
            return Object.assign({}, this, {
                adate: this.adate ? (this.adate instanceof Date ?
                    this.adate.toJSON() : undefined) : undefined,
                udate: this.udate ?  (this.udate instanceof Date ?
                    this.udate.toJSON() : undefined) : undefined,
            });
    
        }
        */

    public static StripHtml(value: string): string {
        let rv: string;
        if (!value) {
            return rv;
        }
        const div = document.createElement('div');
        div.innerHTML = value;
        rv = div.textContent;

        return rv;
    }

    public static CharactersCount(value: string): number {
        let characters = 0;
        if (!value) {
            return characters;
        }
        const text = ChapterModel.StripHtml(value);
        if (text) {
            characters = text.length;
        }

        return characters;
    }

    public static WordCount(value: string): ChapteStat {
        let rv: ChapteStat;
        if (!value) {
            return rv;
        }
        rv = new ChapteStat();
        const text = ChapterModel.StripHtml(value);
        if (text) {
            const words: string[] = text.split(' ');
            rv.wordCount = words.length;
            let shortWCount = 0;
            let longWCount = 0;
            let singleLetterCount = 0;
            words.forEach(word => {
                (word.length > 3) ? longWCount++ : ((word.length > 1) ? shortWCount++ : singleLetterCount++);
            });

            rv.wordCount = words.length;
            rv.longWordCount = longWCount;
            rv.shortWordCount = shortWCount;
            rv.charsCount = text.length;
            rv.singleLetterWordCount = singleLetterCount;

            console.log('ChapteStat->', rv);
        }

        return rv;
    }

    public static getPages(value: string, pageCharts: number = 2500): ChaptePage[] {
        const rv = new Array<ChaptePage>();
        if (!value) {
            return rv;
        }
        // const chStat: ChapteStat = ChapterModel.WordCount(value);


        if (value) {
            const words: string[] = value.split(' ');
            let page: ChaptePage;
            let startWord = 0;
            let currWord = 0;
            let pageContent = '';

            words.forEach(word => {
                if ((pageContent.length + word.length + 1) >= pageCharts) {
                    page = new ChaptePage();
                    page.charsCount = pageContent.length;
                    page.startWord = startWord;
                    page.content = pageContent;
                    page.endWord = currWord;
                    page.pageNr = rv.length + 1;

                    rv.push(page);

                    startWord = currWord;
                    pageContent = '';
                }

                currWord++;
                pageContent += word + ' ';

            });

            if (pageContent && pageContent.length > 0) {
                page = new ChaptePage();
                page.charsCount = pageContent.length;
                page.startWord = startWord;
                page.content = pageContent;
                page.endWord = currWord;
                page.pageNr = rv.length + 1;

                rv.push(page);

                startWord = currWord;
                pageContent = '';
            }

            // console.log('ChapteStat->', rv);
        }

        return rv;
    }
}

export interface IChapterModel {

    bch_id: number;
    bch_title: string;
    bch_subtitle: string;
    bch_notes: string;
    book_id: number;
    bch_content: string;
    bch_order: number;
    adate: string;
    udate: string;
}


export class ChapterList {
    book_chapter_list: Array<IChapterModel>;
}

export interface ResponseChapterList extends ResponseModel {
    data: ChapterList;
}

export class ChapterEdit {
    chapter: IChapterModel;
}

export interface ResponseChapterEdit extends ResponseModel {
    data: ChapterEdit;
}


//#endregion

