import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {  tap, catchError, map } from 'rxjs/operators';
// Custom
import { ApiHelper } from '../api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
import { ServiceNames } from '../api.constants';
import { BaseService } from '../api.service';
import { StorageKeysLocal } from '../../const/bwt-storage-key.const';
import {  CharacterModel, ResponseCharacterList, ResponseCharacterEdit } from './dto';


@Injectable()
export class CharacterService extends BaseService {

    protected observable: Observable<any>;

    constructor(private apiHelper: ApiHelper,
        private http: HttpClient,
        private cus: CurrentUserService) {
        super();
    }

    //#region Data

    private get characters(): Array<CharacterModel> {
        let items = new Array<CharacterModel>();
        const data = localStorage.getItem(StorageKeysLocal.Characters);
        if (data) {
            items = JSON.parse(data);
        }
        return items;
    }

    public resetCharacterCache(): void {
        localStorage.removeItem(StorageKeysLocal.Characters);
    }

    private updateCharacterCashe(data: Array<CharacterModel>): void {

        if (data) {
            const items = this.characters;
            data.forEach((element: CharacterModel) => {
                const toUpdate = items.find(x => x.crtr_id === element.crtr_id);

                if (toUpdate) {
                    const idx = items.indexOf(toUpdate);
                    items[idx] = element;
                } else {
                    items.push(element);
                }
            });

            localStorage.setItem(StorageKeysLocal.Characters, JSON.stringify(items));
        }
    }

    private characterFromCache(id: number): Array<CharacterModel> {
        let data = new Array<CharacterModel>();
        if (id) {
            const cacheData = this.characters;
            if (cacheData && cacheData.length > 0) {
                data = cacheData.filter(x => x.crtr_id === id);
            }
        }

        return data;
    }
    private bookCharactersFromCache(bookId: number): Array<CharacterModel> {
        let data = new Array<CharacterModel>();
        if (bookId) {
            const cacheData = this.characters;
            if (cacheData && cacheData.length > 0) {
                data = cacheData.filter(x => x.book_id === bookId);
            }
        }

        return data;
    }

    public character(id: number): Observable<Array<CharacterModel>> {

        let data = new Array<CharacterModel>();
        if (id) {
            data = this.characterFromCache(id);
            if (!data || data.length === 0) {

                this.observable = this.getCharacterList().pipe(
                    map((responce: ResponseCharacterList) => {
                        this.observable = undefined;
                        data = new Array<CharacterModel>();
                        if (responce.data.book_chrtr_list) {
                            responce.data.book_chrtr_list.forEach((element: CharacterModel) => {
                                // const tmp: BookModel = BookModel.fromJSON(element);
                                data.push(element);
                            });
                            this.updateCharacterCashe(data);
                            data = this.characterFromCache(id);
                        }

                        return data;
                    }),
                    tap(res => { this.log(`getCharacterList`); }),
                    catchError(this.handleError('getBookList', []))
                );

            } else {
                // console.log('data already available');
                return of(data);
            }
        }

        return this.observable;
    }

    public bookCharacters(bookId: number): Observable<Array<CharacterModel>> {

        let data = new Array<CharacterModel>();
        if (bookId) {
            data = this.bookCharactersFromCache(bookId);
            if (!data || data.length === 0) {

                this.observable = this.getBookCharacterList(bookId).pipe(
                    map((responce: ResponseCharacterList) => {
                        this.observable = undefined;
                        data = new Array<CharacterModel>();
                        if (responce.data.book_chrtr_list) {
                            responce.data.book_chrtr_list.forEach((element: CharacterModel) => {
                                // const tmp: BookModel = BookModel.fromJSON(element);
                                data.push(element);
                            });
                            this.updateCharacterCashe(data);
                            data = this.bookCharactersFromCache(bookId);
                        }

                        return data;
                    }),
                    tap(event => {}),
                    catchError(this.handleError)
                );
                /*
                    .map((responce: ResponseCharacterList) => {
                        this.observable = undefined;
                        data = new Array<CharacterModel>();
                        if (responce.data.book_chrtr_list) {
                            responce.data.book_chrtr_list.forEach((element: CharacterModel) => {
                                // const tmp: BookModel = BookModel.fromJSON(element);
                                data.push(element);
                            });
                            this.updateCharacterCashe(data);
                            data = this.bookCharactersFromCache(bookId);
                        }

                        return data;
                    })
                    .share();*/

            } else {
                this.observable = new Observable<Array<CharacterModel>>(observer => {
                    return observer.next(data);
                  });
                // console.log('data already available');
                // return Observable.of(data);
            }
        }

        return this.observable;
    }

    public charactersList(): Observable<Array<CharacterModel>> {

        let data = this.characters;
        if (!data || data.length === 0) {

            this.observable = this.getCharacterList().pipe(
                map((responce: ResponseCharacterList) => {
                    this.observable = undefined;
                    data = new Array<CharacterModel>();
                    if (responce.data.book_chrtr_list) {
                        responce.data.book_chrtr_list.forEach((element: CharacterModel) => {
                            // const tmp: BookModel = BookModel.fromJSON(element);
                            data.push(element);
                        });
                        this.updateCharacterCashe(data);
                    }

                    return data;
                }),
                tap(res => { this.log(`getCharacterList`); }),
                catchError(this.handleError())
            );

        } else {
            // console.log('data already available');
            return of(data);
        }

        return this.observable;
    }

    //#endregion

    //#region Runway Http Calls

    public getCharacterList(): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/character_list',
            { 'user_id': (this.cus.user ? this.cus.user.id : -1) }, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getBookList`); }),
                catchError(this.handleError('getBookList', []))
            );
    }

    public getBookCharacterList(bookId): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/book_character_list',
            { 'book_id': bookId }, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getBookList`); }),
                catchError(this.handleError('getBookList', []))
            );
    }

    public save(myCharacter: CharacterModel): Observable<ResponseCharacterEdit> {
        if (myCharacter) {
            const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
            return this.http.post(this.sertviceUrl + '/book_character_save',
                {crtr: myCharacter},
                { headers: hdrs }).pipe(
                    map((res: ResponseCharacterEdit) => res),
                    tap(event => {
                        this.log(`tap character save event: ` +  JSON.stringify(event));
                        // There may be other events besides the response.
                        if (event instanceof HttpResponse) {
                          // cache.put(req, event); // Update the cache.
                        }
                      }),
                );
        }
    }

    //#endregion

    //#region URL Methods

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.book);
    }

    //#endregion
}
