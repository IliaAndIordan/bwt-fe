import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
// Custom
import { ApiHelper } from '../api.helper';
import { CurrentUserService } from '../../auth/current-user.service';
import { ServiceNames } from '../api.constants';
import { BaseService } from '../api.service';
import { StorageKeysLocal } from '../../const/bwt-storage-key.const';
import { ChapterModel, ResponseChapterList, IChapterModel, ResponseChapterEdit } from './dto';


@Injectable()
export class ChapterService extends BaseService {

    protected observable: Observable<any>;

    constructor(private apiHelper: ApiHelper,
        private http: HttpClient,
        private cus: CurrentUserService) {
        super();
    }

    //#region Data

    private get chapters(): Array<ChapterModel> {
        let items = new Array<ChapterModel>();
        const data = localStorage.getItem(StorageKeysLocal.Chapters);
        if (data) {
            items = JSON.parse(data);
        }
        return items;
    }

    public resetChapterCache(): void {
        localStorage.removeItem(StorageKeysLocal.Chapters);
    }

    private updateChapterCashe(data: Array<ChapterModel>): void {

        if (data) {
            const items = this.chapters;
            data.forEach((element: ChapterModel) => {
                const toUpdate = items.find(x => x.bch_id === element.bch_id);

                if (toUpdate) {
                    const idx = items.indexOf(toUpdate);
                    items[idx] = element;
                } else {
                    items.push(element);
                }
            });

            localStorage.setItem(StorageKeysLocal.Chapters, JSON.stringify(items));
        }
    }

    private chapterFromCache(id: number): Array<ChapterModel> {
        let data = new Array<ChapterModel>();
        if (id) {
            const cacheData = this.chapters;
            if (cacheData && cacheData.length > 0) {
                data = cacheData.filter(x => x.bch_id === id);
            }
        }

        return data;
    }
    private bookChaptersFromCache(bookId: number): Array<ChapterModel> {
        let data = new Array<ChapterModel>();
        if (bookId) {
            const cacheData = this.chapters;
            if (cacheData && cacheData.length > 0) {
                data = cacheData.filter(x => x.book_id === bookId);
            }
        }

        return data;
    }

    public chapter(id: number): Observable<Array<ChapterModel>> {

        let data = new Array<ChapterModel>();
        if (id) {
            data = this.chapterFromCache(id);
            if (!data || data.length === 0) {

                this.observable = this.getBookChapterList(this.cus.selectedBook.id).pipe(
                    map((responce: ResponseChapterList) => {
                        this.observable = undefined;
                        data = new Array<ChapterModel>();
                        if (responce.data.book_chapter_list) {
                            responce.data.book_chapter_list.forEach((element: IChapterModel) => {
                                const tmp: ChapterModel = ChapterModel.fromJSON(element);
                                data.push(tmp);
                            });
                            this.updateChapterCashe(data);
                            data = this.chapterFromCache(id);
                        }

                        return of(data);
                    }),
                    tap(res => { this.log(`getBookChapterList`); }),
                    catchError(this.handleError())
                );

            } else {
                // console.log('data already available');
                return of(data);
            }
        }

        return this.observable;
    }

    public bookChapters(bookId: number): Observable<Array<ChapterModel>> {

        let data = new Array<ChapterModel>();
        if (bookId) {
            data = this.bookChaptersFromCache(bookId);
            if (!data || data.length === 0) {

                this.observable = this.getBookChapterList(bookId).pipe(
                    map((responce: ResponseChapterList) => {
                        this.observable = undefined;
                        data = new Array<ChapterModel>();
                        if (responce.data.book_chapter_list) {
                            responce.data.book_chapter_list.forEach((element: IChapterModel) => {
                                const tmp: ChapterModel = ChapterModel.fromJSON(element);
                                data.push(tmp);
                            });
                            this.updateChapterCashe(data);
                            data = this.bookChaptersFromCache(bookId);
                        }

                        return data;
                    }),
                    tap(res => { this.log(`getBookChapterList`); }),
                    catchError(this.handleError())
                );

            } else {
                // console.log('data already available');
                return of(data);
            }
        }

        return this.observable;
    }

    //#endregion

    //#region Runway Http Calls

    public getBookChapterList(bookId): Observable<any> {

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.http.post(this.sertviceUrl + '/book_chapter_list',
            { 'book_id': bookId }, { headers: hdrs })
            .pipe(
                tap(res => { this.log(`getBookChapterList`); }),
                catchError(this.handleError('getBookChapterList', []))
            );
    }

    public save(myChapter: ChapterModel): Observable<Array<ChapterModel>> {
        let data = new Array<ChapterModel>();
        if (myChapter) {
            const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
            this.observable = this.http.post(this.sertviceUrl + '/book_chapter_save',
                { chapter: myChapter },
                { headers: hdrs }).pipe(
                    map((responce: ResponseChapterEdit) => {
                        data = new Array<ChapterModel>();

                        if (responce.data.chapter) {
                            const tmp: ChapterModel = ChapterModel.fromJSON(responce.data.chapter);
                            data.push(tmp);
                            this.updateChapterCashe(data);
                            this.chapter(responce.data.chapter.bch_id).subscribe(res => {
                                return res;
                            });
                        }

                        return data;
                    }),
                    tap(event => {
                        this.log(`tap chapter save event: ` + JSON.stringify(event));
                        // There may be other events besides the response.
                        if (event instanceof HttpResponse) {
                            // cache.put(req, event); // Update the cache.
                        }
                    }),
                );
        }
        return this.observable;
    }

    //#endregion

    //#region URL Methods

    private get sertviceUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.book);
    }

    //#endregion
}
