

export class DateModel {
    date: Date;
    timezone: string;
    timezone_type: number;

    public static fromJSON(json: IDateModel): DateModel {
        const dm = Object.create(DateModel.prototype);
        return Object.assign(dm, json, {
            date: (json && json.date) ? new Date(json.date) : undefined
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? DateModel.fromJSON(value) : value;
    }

    public toJSON(): IDateModel {
        const tzoffset = (new Date()).getTimezoneOffset() * 60000;
        return Object.assign({}, this, {
            date: this.date ? new Date(this.date.getTime() -
                (this.date.getTimezoneOffset() * 60000)).toISOString()
                .slice(0, 19).replace('T', ' ') : undefined
        });
    }
}

export interface IDateModel {
    date: string;
    timezone: string;
    timezone_type: number;
}
