import { Route, PreloadingStrategy } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from "@angular/core";

// add data:{preload:true} to lazy loaded module
@Injectable()
export class PreloadSelectedModuleList implements PreloadingStrategy {
    preload(route: Route, load: Function): Observable<any> {
        return route.data && route.data.preload ? load() : null;
    }
}
