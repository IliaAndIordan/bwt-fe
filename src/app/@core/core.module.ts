import { NgModule, Optional, SkipSelf } from '@angular/core';
import { JwtModule, JWT_OPTIONS, JwtHelperService, JwtInterceptor } from '@auth0/angular-jwt';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { GravatarModule } from '@infinitycube/gravatar';

import { throwIfAlreadyLoaded } from './guards/module-import.gard';

import { TokenService } from './auth/token.service';
import { TokenFactory } from './auth/token-factory';
import { ApiHelper } from './api/api.helper';
import { AuthService } from './auth/api/auth.service';
import { CurrentUserService } from './auth/current-user.service';

import { AuthGuard } from './guards/aut-guard.service';
import { BookService } from './api/book/book.service';
import { AutorService } from './api/user/autor.service';
import { CharacterService } from './api/book/character.service';
import { ChapterService } from './api/book/chapter.service';
import { TokenInterceptor } from './auth/token.interseptor';
import { CommonModule } from '@angular/common';
import { UserRoleDisplayPipe } from './auth/api/enums';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        /*GravatarModule,*/
        JwtModule.forRoot({
            jwtOptionsProvider: {
                provide: JWT_OPTIONS,
                useClass: TokenFactory
            }
        }),
        GravatarModule,
    ],
    declarations:[
    ],
    exports: [
    ],
    providers: [
        ApiHelper,
        TokenService,
        AuthService,
        JwtInterceptor, // Providing JwtInterceptor allow to inject JwtInterceptor manually into RefreshTokenInterceptor
        {
          provide: HTTP_INTERCEPTORS,
          useExisting: JwtInterceptor,
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true
        },
        TokenFactory,
        CurrentUserService,
        AuthGuard,
        /*SpinnerService,
        */
       BookService,
       AutorService,
       CharacterService,
       ChapterService,
    ]
})

export class BwtCoreModule {
    constructor(@Optional() @SkipSelf() parentModule: BwtCoreModule) {
        throwIfAlreadyLoaded(parentModule, 'BwtCoreModule');
    }
}
