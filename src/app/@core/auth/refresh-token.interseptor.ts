import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpClient, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiHelper } from '../api/api.helper';
import { ServiceNames } from '../api/api.constants';

/**
 * And you'll have to register the following provider :
 *
 *  {
 *   provide: HTTP_INTERCEPTORS,
    useClass: RefreshTokenInterceptor,
    multi: true
}
 */


/**
 * Support token refresh
 * https://github.com/auth0/angular2-jwt/issues/15
 *
 * @Inject(MAGGLE_CONFIG) private maggleConfig: MaggleConfig,
 * private ngRedux: NgRedux<IMaggleState>,
 * private tokenActions: TokenActions
 */
@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
    jwtHelper: JwtHelperService;

    constructor(private router: Router,
        private http: HttpClient,
        private apiHelper: ApiHelper) {
        this.jwtHelper = new JwtHelperService();
    }

    private get refreshUrl(): string {
        return this.apiHelper.getServiceUrl(ServiceNames.base);
    }


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const refreshUrl = this.refreshUrl;

        const token = localStorage.getItem('access_token');
        const refreshToken = localStorage.getItem('refresh_token');

        if ((!token && !refreshToken) || request.url === refreshUrl) {
            // continue normally
            // return next.handle(request).catch(() => this.redirectToLogin());
        }

        if (this.jwtHelper.isTokenExpired(token)) {
            /*
            return this.ngRedux.select<boolean>('refreshingToken').first().flatMap(refreshing => {
                if (refreshing) {

                    // Wait for new token before sending the request
                    return this.ngRedux.select<boolean>('refreshingToken').skip(1).flatMap(refreshing => {
                        return next.handle(request).catch(() => this.redirectToLogin());
                    });

                } else {

                    // Refresh token if expired and not already refreshing
                    this.ngRedux.dispatch(<any>this.tokenActions.refreshToken(true));
                    return this.http.post(refreshUrl, { refresh_token: refreshToken }).flatMap(
                        res => {
                            localStorage.setItem('access_token', res['token']);
                            localStorage.setItem('refresh_token', res['refresh_token']);
                            this.ngRedux.dispatch(<any>this.tokenActions.refreshToken(false));
                            return next.handle(request).catch(() => this.redirectToLogin());
                        }).catch(() => this.redirectToLogin());
                }
            });
            */
        } else {
            return next.handle(request);
        }
    }

    redirectToLogin(): Observable<HttpEvent<any>> {
        /*
        this.router.navigate(this.maggleConfig.loginRoute);
        localStorage.removeItem('refresh_token');
        localStorage.removeItem('access_token');
        this.ngRedux.dispatch(<any>this.tokenActions.refreshToken(false));
        */
        return Observable.of(undefined);
    }
}
