import { Injectable, Inject } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenFactory } from './token-factory';
import { StorageKeysSession } from '../const/bwt-storage-key.const';
import { RefreshTokenModel } from './api/dto';

@Injectable()
export class TokenService extends TokenFactory {


  constructor(private jwtHelper: JwtHelperService) {
    super();
  }

  //#region JwtHelperService methods

  public isTokenExpired(): boolean {
    const retVal = this.jwtHelper.isTokenExpired();
    if (retVal) {
      // console.log('isTokenExpired: ' + retVal);
    }
    return retVal;
  }

  public isAccessTokenExpired(): boolean {
    return this.isTokenExpired();
  }

  public isRefreshTokenExpired(): boolean {
    let rv = true;
    if (this.refreshToken) {
      const refToken: RefreshTokenModel = Object.assign(new RefreshTokenModel(), this.decode(this.refreshToken));
      // console.log('isRefreshTokenExpired-> refToken:', refToken);
      if (refToken) {
        const curDate: Date = new Date();
        // console.log('isRefreshTokenExpired-> curDate ms:', curDate.getTime());
        const diff_ms = ((refToken.exp * 1000) - (curDate.getTime() + 500));
        // console.log('isRefreshTokenExpired-> diff_ms:', diff_ms.toFixed());
        rv = diff_ms > 0 ? false : true;
      }
    }


    return rv;
  }

  public getTokenExpirationDate(): Date {
    return this.jwtHelper.getTokenExpirationDate();
  }

  public decodeToken(): string {
    return this.jwtHelper.decodeToken();
  }

  public decode(token: string): string {
    return this.jwtHelper.decodeToken(token);
  }

  public get expIntervalMs(): number {
    let rv = 0;
    const curDate: Date = new Date();
    const jwtExp: Date = this.getTokenExpirationDate();
    rv = jwtExp.getTime() - curDate.getTime();
    // console.log('ExpIntervalMs-> rv=', rv);
    return rv;
  }

  //#endregion

  //#region Token

  public set barerToken(value: string) {
    if (!value) {
      return;
    }

    sessionStorage.setItem(StorageKeysSession.BearerToken, value);
  }

  public get barerToken(): string {
    return sessionStorage.getItem(StorageKeysSession.BearerToken);
  }


  public get refreshToken(): string {
    return sessionStorage.getItem(StorageKeysSession.RefreshToken);
  }

  public set refreshToken(value: string) {
    if (!value) {
      return;
    }
    sessionStorage.setItem(StorageKeysSession.RefreshToken, value);
  }

  public clearToken() {
    sessionStorage.removeItem(StorageKeysSession.BearerToken);
    sessionStorage.removeItem(StorageKeysSession.RefreshToken);
  }

  //#endregion

}
