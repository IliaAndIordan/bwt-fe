import { DateModel, IDateModel } from '../../models/date.model';
import { UserRole } from './enums';


export class RefreshTokenModel {
    client_id: number;
    exp: number;
    iss: string;
    guid: string;
}

export class UserModel {

    public id: number;
    public eMail: string;
    public name: string;
    // 2 predefined roles 1-user, 3 admin, 2 editor
    public role: UserRole;
    public roleName: string;
    public isReceiveEMails: boolean;
    public ipAddress: string;
    public lastLogged: DateModel;
    public updated: DateModel;


    public static fromJSON(json: IUserModel): UserModel {
        const vs = Object.create(UserModel.prototype);
        return Object.assign(vs, json, {
            id: json.id ? json.id : json.userId ? json.userId : undefined,
            role: json.role ? json.role as UserRole : UserRole.User,
            isReceiveEMails: json.isReceiveEMails === 1 ? true : false,
            lastLogged: DateModel.fromJSON(json.lastLogged),
            updated: DateModel.fromJSON(json.updated),
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {
        return key === '' ? UserModel.fromJSON(value) : value;
    }

    public toJSON(): IUserModel {

        return Object.assign({}, this, {
            isReceiveEMails: this.isReceiveEMails ? 1 : 0,
        });

    }
}

export interface IUserModel {

    id: number;
    userId: number;
    eMail: string;
    name: string;
    // 2 predefined roles 0-user, 1 admin, 2 editor
    role: number;
    roleName: string;
    isReceiveEMails: number;
    ipAddress: string;
    lastLogged: IDateModel;
    updated: IDateModel;
}



export class ResponseAuthenticateUser {
    public current_user: IUserModel;
    public refreshToken: string;
}

export class ResponseAuthenticate {
    public status: string;
    public data: ResponseAuthenticateUser;
    public message: string;
}
