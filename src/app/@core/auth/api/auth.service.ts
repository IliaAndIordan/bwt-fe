import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

// Services
import { ApiBaseClient } from '../../api/api-base.client';
import { TokenService } from '../token.service';
import { CurrentUserService } from '../current-user.service';
import { UserModel, ResponseAuthenticate } from './dto';
import { Router } from '@angular/router';
// Models
import { BwtRoutes } from '../../const/bwt-routes.const';
import { StorageKeysSession } from '../../const/bwt-storage-key.const';

@Injectable()
export class AuthService extends ApiBaseClient {

  private baseUrl = 'https://ws.bwt.iordanov.info/';

  private registerUrl = this.baseUrl + 'register/';
  protected observable: Observable<any>;

  constructor(
    private router: Router,
    private client: HttpClient,
    private tokenService: TokenService,
    private cus: CurrentUserService) {
    super();
  }



  //#region JWT Tocken

  register(email: string, password: string): Observable<any> {
    // console.log('e-mail: ' + email + ', psw: ' + password);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.registerUrl,
      { email: email, password: password },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => this.convertAutenticateToUser(responce)),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError)
      );
    /*
      .map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      });
      */
  }

  autenticate(username: string, password: string): Observable<any> {
    // console.log('user: ' + username + ', ped: ' + password);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + 'login',
      { username: username, password: password },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => this.convertAutenticateToUser(responce)),
        /* tap((res: HttpResponse<any>) => { console.log(res.headers.keys); }),*/
        catchError(this.handleError)
      );
  }

  convertAutenticateToUser(response: HttpResponse<any>): ResponseAuthenticate {
    let userObj = null;
    let token = null;
    let rv: ResponseAuthenticate;
    // console.log('response:', response);
    if (response) {
      if (response.headers) {
        token = response.headers.get('X-Authorization');
      }

      if (response.body) {
        rv = Object.assign(new ResponseAuthenticate(), response.body);
        // console.log('convertAutenticateToUser-> rv: ', rv);
        if (rv.data && rv.data.current_user) {
          userObj = UserModel.fromJSON(rv.data.current_user);
          // Object.assign(new UserModel(), rv.data.current_user);
          console.log('convertAutenticateToUser-> User: ', userObj);
        } else {
          console.log('Autentication Failed. res:', rv.message);
        }
      }

      if (!this.cus.user || this.cus.user.id !== userObj.id) {
        // this.cus.clearLocalStorage();
      }


      if (token) {
        this.tokenService.barerToken = token;
        // sessionStorage.setItem(StorageKeysSession.BearerToken, token);
        /*
        if (!this.refreshTimer) {
          // console.log('tokenRefreshIntervalMs:', this.cus.tokenRefreshIntervalMs);
          this.refreshTimer = interval(this.cus.tokenRefreshIntervalMs);

          this.refreshSubscriber = this.refreshTimer.subscribe(sequenceNr => {
            // console.log(`It's been ${sequenceNr} refreshTimer call!`);
            this.refreshTimer = interval(this.cus.tokenRefreshIntervalMs);
            this.refreshToken()
              .subscribe(res => {
                console.log('refreshToken res:', res);
              });
          });
        }
        */
        // console.log(' ---- decode');
        // console.log(this.tokenService.decodeToken());
      }
      this.cus.user = userObj;
    }

    if (rv && rv.data && rv.data.refreshToken) {
      this.tokenService.refreshToken = rv.data.refreshToken;
      console.log('convertAutenticateToUser-> refreshToken: ', this.tokenService.decode(this.tokenService.refreshToken));
    }
    // console.dir(retvalue);
    return rv;
  }

  refreshToken(): Observable<any> {

    console.log('refreshToken -> ');
    // console.log('refreshToken -> barerToken:', this.tokenService.barerToken);
    // console.log('refreshToken -> refreshToken:', this.tokenService.refreshToken);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + 'jwtrefresh',
      { refresh: this.tokenService.refreshToken },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((response: HttpResponse<any>) => {
          const ra: ResponseAuthenticate = this.convertAutenticateToUser(response);
          console.log('refreshToken -> ResponseAuthenticate:', ra);
          // console.log('refreshToken -> barerToken:', this.tokenService.barerToken);
          return ra;
        }),
        catchError(this.handleError)
      );
  }



  logout() {
    // remove user from local storage to log user out
    // sessionStorage.removeItem(StorageKeysSession.BearerToken);

    this.cus.user = undefined;

    this.cus.clearLocalStorage();
    this.cus.clearSession();

    this.router.navigate(['/', BwtRoutes.Public]);
  }

}
