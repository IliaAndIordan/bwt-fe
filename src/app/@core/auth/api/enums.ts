import { Pipe, PipeTransform } from '@angular/core';
import { EnumViewModel } from '../../models/common/enum-view.model';

export enum UserRole {
    User = 1,
    Editor = 2,
    Admin = 3
}

export const UserRoleOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Author'
    },
    {
        id: 2,
        name: 'Editor'
    },
    {
        id: 3,
        name: 'Admin'
    },
];


@Pipe({ name: 'userrole' })
export class UserRoleDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = UserRole[value];
        const data: EnumViewModel = UserRoleOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}
