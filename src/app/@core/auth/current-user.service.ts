import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

// Models
import { UserModel } from './api/dto';
import { LocalUserSettings } from './local-user-settings';
import { TokenService } from './token.service';
import { GravatarService } from '@infinitycube/gravatar';
import { AuthService } from './api/auth.service';
import { StorageKeysLocal, StorageKeysSession } from '../const/bwt-storage-key.const';
import { BookModel, ChapterModel } from '../api/book/dto';
import { Router } from '@angular/router';
import { BwtRoutes } from '../const/bwt-routes.const';
import { UserRole } from './api/enums';

@Injectable()
export class CurrentUserService {

    constructor(private router: Router,
        private tockenService: TokenService,
        private gravatarService: GravatarService) { }

    //#region User

    public UserChanged = new Subject<UserModel>();


    public get user(): UserModel {
        const userStr = sessionStorage.getItem(StorageKeysSession.CurrentUser);
        let userObj = null;
        if (userStr) {
            userObj = Object.assign(new UserModel(), JSON.parse(userStr));
        }
        return userObj;
    }

    public set user(userObj: UserModel) {
        if (userObj) {
            sessionStorage.setItem(StorageKeysSession.CurrentUser, JSON.stringify(userObj));
        } else {
            sessionStorage.removeItem(StorageKeysSession.CurrentUser);
        }
        this.UserChanged.next(userObj);
    }

    public get isAuthenticated(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        retValue = (userObj) ? true : false;
        // console.log('isLogged: ' + retValue);
        return retValue;
    }

    public get isAdmin(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        if (userObj) {
            retValue = (userObj.role === UserRole.Admin);
        }
        // console.log('isLogged: ' + retValue);
        return retValue;
    }

    public get avatarUrl(): string {
        let retValue = './../../assets/images/common/iziordanov_sd_128_bw.png';
        const userObj: UserModel = this.user;

        if (userObj && userObj.eMail) {
            const gravatarImgUrl = this.gravatarService.url(userObj.eMail, 128, 'identicon');
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    clearLocalStorage() {
        console.log('clearLocalStorage ->');
        Object.keys(StorageKeysLocal).forEach((key: string) => {
            if (StorageKeysLocal.hasOwnProperty(key)) {
                const val = (StorageKeysLocal as any)[key];
                // console.log('clearLocalStorage key= ' + key + ', val= ' + val);
                localStorage.removeItem(val);
            }
        });
        // console.log('clearLocalStorage <-');
    }

    clearSession() {
        console.log('clearSession ->');
        Object.keys(StorageKeysSession).forEach((key: string) => {
            if (StorageKeysSession.hasOwnProperty(key)) {
                const val = (StorageKeysSession as any)[key];
                // console.log('clearLocalStorage key= ' + key + ', val= ' + val);
                sessionStorage.removeItem(val);
            }
        });
        // console.log('clearLocalStorage <-');
    }

    //#endregion

    //#region Local User Settings

    // tslint:disable-next-line:member-ordering
    public LocalSettingsChanged = new Subject<LocalUserSettings>();

    public get localSettings(): LocalUserSettings {
        const userStr = sessionStorage.getItem(StorageKeysSession.LocalUserSettings);
        let userObj = null;
        if (userStr) {
            userObj = Object.assign(new LocalUserSettings(), JSON.parse(userStr));
        } else {
            userObj = new LocalUserSettings();
            userObj.snavLeftWidth = 4;
            sessionStorage.setItem(StorageKeysSession.LocalUserSettings, JSON.stringify(userObj));
        }
        return userObj;
    }

    public set localSettings(userObj: LocalUserSettings) {
        if (userObj) {
            sessionStorage.setItem(StorageKeysSession.LocalUserSettings, JSON.stringify(userObj));
        } else {
            sessionStorage.removeItem(StorageKeysSession.LocalUserSettings);
        }
        this.LocalSettingsChanged.next(userObj);
    }

    //#endregion

    //#region  Selected book

    public SelectedBookChanged = new Subject<BookModel>();

    public get selectedBook(): BookModel {
        let rv;
        const bookJson = localStorage.getItem(StorageKeysLocal.SelBook);
        if (bookJson) {
            rv = BookModel.fromJSON(JSON.parse(bookJson));
        }
        return rv;
    }

    public set selectedBook(obj: BookModel) {
        if (obj) {
            localStorage.setItem(StorageKeysLocal.SelBook, JSON.stringify(obj));
        } else {
            localStorage.removeItem(StorageKeysLocal.SelBook);
        }
        this.SelectedBookChanged.next(obj);
    }

    //#endregion

    //#region  Selected Chapter

    public SelectedChapterChanged = new Subject<ChapterModel>();

    public get selectedChapter(): ChapterModel {
        let rv;
        const json = localStorage.getItem(StorageKeysLocal.SelChapter);

        if (json) {
            rv = ChapterModel.fromJSON(JSON.parse(json));

        }
        return rv;
    }

    public set selectedChapter(obj: ChapterModel) {

        if (obj) {

            localStorage.setItem(StorageKeysLocal.SelChapter, JSON.stringify(obj));
        } else {

            localStorage.removeItem(StorageKeysLocal.SelChapter);
        }
        this.SelectedChapterChanged.next(obj);
    }

    //#endregion


}
