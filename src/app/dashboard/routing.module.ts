import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// --- Constants

// Local Components
import { HomeComponent } from './home.component';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
    {path: '', component: HomeComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class  DashboardRoutingModule { }

export const RoutedComponents = [ DashboardComponent, HomeComponent];
