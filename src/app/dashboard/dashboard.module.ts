import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { LayoutModule } from '@angular/cdk/layout';
import { RoutedComponents,  DashboardRoutingModule } from './routing.module';
import { SharedModule } from '../@share/shared.module';
import { BwtMaterialModule } from '../@share/material/material.module';
import { BwtCoreModule } from '../@core/core.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BwtMaterialModule,
    MatGridListModule,
    DashboardRoutingModule,
  ],
  declarations: [RoutedComponents]
})
export class DashboardModule { }
