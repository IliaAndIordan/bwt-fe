import { Component, ViewContainerRef, OnDestroy, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { CurrentUserService } from '../@core/auth/current-user.service';
import { BookModel } from '../@core/api/book/dto';
import { BookService } from '../@core/api/book/book.service';
import { UserModel } from '../@core/auth/api/dto';
import { Router } from '@angular/router';
import { BwtRoutes } from '../@core/const/bwt-routes.const';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})

export class HomeComponent implements OnInit, OnDestroy {

  colspan4Card = 3;
  bookList: Array<BookModel>;

  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Card 1', cols: 1, rows: 1 },
          { title: 'Card 2', cols: 1, rows: 1 },
          { title: 'Card 3', cols: 1, rows: 1 },
          { title: 'Card 4', cols: 1, rows: 1 }
        ];
      }

      return [
        { title: 'Card 1', cols: 2, rows: 1 },
        { title: 'Card 2', cols: 1, rows: 1 },
        { title: 'Card 3', cols: 1, rows: 2 },
        { title: 'Card 4', cols: 1, rows: 1 }
      ];
    })
  );

  user: UserModel;
  isAuthenticated: boolean;
  userChangedSubsc: any;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private vcr: ViewContainerRef,
    private cus: CurrentUserService,
    private bookService: BookService) {

  }

  ngOnInit(): void {
    this.user = this.cus.user;
    this.isAuthenticated = this.cus.isAuthenticated;
    this.userChangedSubsc = this.cus.UserChanged.subscribe((user: UserModel) => {
      this.isAuthenticated = this.cus.isAuthenticated;
      this.user = this.cus.user;
      // console.log('userChangedSubsc-> isAuthenticated:', this.cus.isAuthenticated);
      if (this.isAuthenticated) {
        this.loadBookList();
      }  else {
        this.router.navigate(['/', BwtRoutes.Public]);
      }
    });
    this.loadBookList();
  }

  ngOnDestroy(): void {
    if (this.userChangedSubsc) { this.userChangedSubsc.unsubscribe(); }
  }

  breakpointObserverInit(): void {
    this.breakpointObserver
      .observe([Breakpoints.Handset])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Handset viewport  mode');
          this.colspan4Card = 12;
        }
      });
    this.breakpointObserver
      .observe([Breakpoints.Tablet])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Tablet viewport mode');
          this.colspan4Card = 6;
        }
      });

    this.breakpointObserver
      .observe([Breakpoints.Web])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Web viewport mode');
          this.colspan4Card = 3;
        }
      });
    this.breakpointObserver
      .observe([Breakpoints.Small])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Handset viewport  mode');
          this.colspan4Card = 12;
        }
      });

  }

  updateFields(): void { }

  //#region Data

  loadBookList(): void {
    this.bookList = new Array<BookModel>();
    if (!this.isAuthenticated) {
      this.router.navigate(['/', BwtRoutes.Public]);
    } else {
      this.bookService.bookList()
        .subscribe((resp: Array<BookModel>) => {
          this.bookList = resp;
          // console.log('loadBookList->', resp);
          this.updateFields();
        });
    }

  }

  reloadBookList(): void {
    this.bookService.resetBookCache();
    this.loadBookList();
  }

  selectBook(book: BookModel): void {

  }

  //#endregion
}
