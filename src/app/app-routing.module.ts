import { NgModule } from '@angular/core';
import { RouterModule, Routes, provideRoutes } from '@angular/router';
// Constants
// ---Components
import { PreloadSelectedModuleList } from './@core/preload-strategy';
import { AppComponent } from './app.component';
import { BwtRoutes } from './@core/const/bwt-routes.const';

export const routes: Routes = [
  { path: BwtRoutes.Root, pathMatch: 'full', redirectTo: BwtRoutes.Public },
  { path: BwtRoutes.Dashboard, loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule), data: { preload: true } },
  { path: BwtRoutes.Public, loadChildren: () => import('./public/public.module').then(m => m.PublicModule), data: { preload: true } },
  { path: BwtRoutes.Book, loadChildren: () => import('./book/book.module').then(m => m.BookModule), data: { preload: true } },
  { path: BwtRoutes.Chapter, loadChildren: () => import('./chapter/chapter.module').then(m => m.ChapterModule), data: { preload: true } },
  { path: BwtRoutes.Profile, loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule), data: { preload: true } },



];

@NgModule({

  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadSelectedModuleList, enableTracing: false, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule],
  providers: [PreloadSelectedModuleList]

})
export class AppRoutingModule { }

export const routableComponents = [];

