import { Component, ViewContainerRef, OnInit, OnDestroy } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { CurrentUserService } from '../@core/auth/current-user.service';
import { BookModel } from '../@core/api/book/dto';
import { BookService } from '../@core/api/book/book.service';
import { UserModel } from '../@core/auth/api/dto';
import { StorageKeysLocal } from '../@core/const/bwt-storage-key.const';
import { Router } from '@angular/router';
import { BwtRoutes } from '../@core/const/bwt-routes.const';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})

export class HomeComponent implements OnInit, OnDestroy {

  user: UserModel;
  isAuthenticated: boolean;
  userChangedSubsc: any;
  avatarImageUrl: string;
  bookList: Array<BookModel>;


  constructor(
    private router: Router,
    private breakpointObserver: BreakpointObserver,
    private vcr: ViewContainerRef,
    private cus: CurrentUserService,
    private bookService: BookService) {
  }

  ngOnInit(): void {

    this.onUserChanged();
    this.userChangedSubsc = this.cus.UserChanged.subscribe((user: UserModel) => {
      this.onUserChanged();
    });
  }

  ngOnDestroy(): void {
    if (this.userChangedSubsc) { this.userChangedSubsc.unsubscribe(); }
  }

  onUserChanged() {
    this.user = this.cus.user;
    this.isAuthenticated = this.cus.isAuthenticated;
    this.avatarImageUrl = this.cus.avatarUrl;
    this.loadBookList();
  }


  breakpointObserverInit(): void {
    this.breakpointObserver
      .observe([Breakpoints.Handset])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Handset viewport  mode');
          //this.colspan4Card = 12;
        }
      });
    this.breakpointObserver
      .observe([Breakpoints.Tablet])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Tablet viewport mode');
          //this.colspan4Card = 6;
        }
      });

    this.breakpointObserver
      .observe([Breakpoints.Web])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Web viewport mode');
          //this.colspan4Card = 3;
        }
      });
    this.breakpointObserver
      .observe([Breakpoints.Small])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Handset viewport  mode');
          //this.colspan4Card = 12;
        }
      });

  }

  updateFields(): void { }

  public selectBookclicked(book: BookModel): void {
    if (book) {
      // console.log('selectBookclicked', book);
      localStorage.setItem(StorageKeysLocal.SelBook, JSON.stringify(book));
      this.router.navigate(['/', BwtRoutes.Book]);
    }

  }


  loadBookList(): void {
    this.bookList = new Array<BookModel>();
    this.bookService.bookList()
      .subscribe((resp: Array<BookModel>) => {
        this.bookList = resp;
        // console.log('loadBookList->', resp);
        this.updateFields();
      });

  }


  //#region Data


  //#endregion
}
