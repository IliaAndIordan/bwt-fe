import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutedComponents, ProfileRoutingModule } from './routing.module';
import { SharedModule } from '../@share/shared.module';
import { BwtMaterialModule } from '../@share/material/material.module';
import { MatGridListModule } from '@angular/material/grid-list';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BwtMaterialModule,
    MatGridListModule,
    ProfileRoutingModule,
  ],
  declarations: [
    RoutedComponents,
  ]
})
export class ProfileModule { }
