import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { MatSidenav } from '@angular/material/sidenav';
import { CurrentUserService } from './@core/auth/current-user.service';
import { LoginModalDialog } from './@share/modal/login/login-modal.dialog';
import { Router } from '@angular/router';
import { BwtRoutes } from './@core/const/bwt-routes.const';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @ViewChild('sideLeft', { static: true }) sideLeft: MatSidenav;

  title = 'Book Writing Tool';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  isTablet$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Tablet)
    .pipe(
      map(result => result.matches)
    );

  isAuthenticated: boolean;
  private userChangedSubscription: any;

  constructor(
    private router: Router,
    private breakpointObserver: BreakpointObserver,
    private currUserService: CurrentUserService,
    public dialogService: MatDialog) {
    this.isAuthenticated = this.currUserService.isAuthenticated;
    this.userChangedSubscription = this.currUserService.UserChanged
      .subscribe(newUser => {
        this.isAuthenticated = this.currUserService.isAuthenticated;

        if (!this.isAuthenticated) {
          this.router.navigate(['/', BwtRoutes.Public]);
        }
      });
  }

  sideLeftToggle() {
    this.sideLeft.toggle();
  }

  //#region Login/User Profile

  public onProfileClick(): void {
    if (this.currUserService.isAuthenticated) {
      this.router.navigate(['/', BwtRoutes.Profile]);
    } else {
      this.loginDialogShow();
    }
  }


  public loginDialogShow(): void {
    const dialogRef = this.dialogService.open(LoginModalDialog, {
      width: '500px',
      height: '320px',
      data: { email: null, password: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
      // this.password = result.password;
    });
  }

  //#endregion
}
