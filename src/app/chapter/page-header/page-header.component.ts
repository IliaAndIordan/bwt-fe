import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';
// Constants
// Services
import { CurrentUserService } from 'src/app/@core/auth/current-user.service';
import { UserModel } from 'src/app/@core/auth/api/dto';
import { AuthService } from 'src/app/@core/auth/api/auth.service';
import { BookModel, ChapterModel } from 'src/app/@core/api/book/dto';
import { BwtRoutes } from 'src/app/@core/const/bwt-routes.const';
import { StorageKeysLocal } from 'src/app/@core/const/bwt-storage-key.const';

@Component({
    selector: 'app-page-book-header',
    templateUrl: './page-header.component.html',
})

export class PageHeaderComponent implements OnInit, OnDestroy {

    @Input() canSave: boolean;
    @Output() contentSave: EventEmitter<any> = new EventEmitter<any>();

    user: UserModel;
    userChanged: any;
    isAdmin: boolean;
    isAutor: boolean;

    book: BookModel;
    chapter: ChapterModel;
    chapterChanged: any;

    constructor(
        private router: Router,
        private cus: CurrentUserService,
        private authService: AuthService) { }

    ngOnInit(): void {
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
        this.getSelBook();
        this.userChanged = this.cus.UserChanged.subscribe(newUser => {
            // console.log('newUser->', newUser);
            this.user = newUser;
            this.isAdmin = this.cus.isAdmin;
            this.getSelBook();
        });

        this.chapterChanged = this.cus.SelectedChapterChanged.subscribe(newChapter => {
            // console.log('newChapter->', newChapter);
            this.getSelBook();
        });

    }

    getSelBook() {
        this.book = this.cus.selectedBook;

        this.chapter = this.cus.selectedChapter;
        this.isAutor = this.book ? (this.book.userId === (this.cus.user ? this.cus.user.id : -1)) : false;
        this.canSave = false;
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
        if (this.chapterChanged) { this.chapterChanged.unsubscribe(); }
    }

    //#region Navigation

    onContentSave() {
        this.contentSave.emit();
    }

    gotoAircraftAdmin() {
        if (this.cus.user && this.cus.isAdmin) {
            this.router.navigate(['/', BwtRoutes.Dashboard]);
        } else {
            this.logout();
        }

    }

    gotoBook() {
        if (this.cus.selectedBook) {
            this.router.navigate(['/', BwtRoutes.Book]);
        } else {
            this.logout();
        }
    }

    gotoBookList() {
        if (this.cus.selectedBook) {
            this.router.navigate(['/', BwtRoutes.Dashboard]);
        } else {
            this.logout();
        }
    }

    logout(): void {
        this.authService.logout();
    }


    //#endregion


}
