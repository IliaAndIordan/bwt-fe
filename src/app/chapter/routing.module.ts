import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// --- Constants

// Local Components
import { HomeComponent } from './home.component';
import { ChapterComponent } from './chapter.component';

const routes: Routes = [
    {path: '', component: HomeComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class  ChapterRoutingModule { }

export const RoutedComponents = [ ChapterComponent, HomeComponent];
