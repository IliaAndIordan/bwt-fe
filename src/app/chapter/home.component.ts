import { Component, ViewContainerRef, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { map, distinctUntilChanged, switchMap, subscribeOn ,  debounceTime } from 'rxjs/operators';

import { Breakpoints, BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { CurrentUserService } from '../@core/auth/current-user.service';
import { BookModel, ChapterModel, ResponseChapterEdit } from '../@core/api/book/dto';
import { BookService } from '../@core/api/book/book.service';
import { UserModel } from '../@core/auth/api/dto';
import { StorageKeysLocal } from '../@core/const/bwt-storage-key.const';
import { AutorService } from '../@core/api/user/autor.service';
import { AutorModel } from '../@core/api/user/dto';
import { AngularEditorConfig, AngularEditorComponent } from '@kolkov/angular-editor';
import { Subject, Observable, pipe } from 'rxjs';
import { ChapterService } from '../@core/api/book/chapter.service';
import { createOfflineCompileUrlResolver } from '@angular/compiler';
import { Router } from '@angular/router';
import { BwtRoutes } from '../@core/const/bwt-routes.const';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})

export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('aneditor', { static: false }) editor: AngularEditorComponent;

  contentChanged = new Subject<string>();
  chapterChanged: any;
  timeout: any;

  colspan4Card = 3;
  colspanGridMainBoard = 8;
  colspanGridMainRSide = 4;
  bpoObsHandset: any;
  bpoObsTablet: any;
  bpoObsWeb: any;
  bpoObssmall: any;

  user: UserModel;
  userChanged: any;
  isAdmin: boolean;
  isAutor: boolean;

  book: BookModel;
  autor: AutorModel;
  chapter: ChapterModel;
  canSave: boolean;



  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    /*height: 'calc(100vh - 210px)',
    minHeight: '5rem',*/
    placeholder: 'Enter text here...',
    translate: 'no',
    toolbarPosition: 'top',
    defaultFontName: 'Times New Roman',
  };

  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Card 1', cols: 1, rows: 1 },
          { title: 'Card 2', cols: 1, rows: 1 },
          { title: 'Card 3', cols: 1, rows: 1 },
          { title: 'Card 4', cols: 1, rows: 1 }
        ];
      }

      return [
        { title: 'Card 1', cols: 2, rows: 1 },
        { title: 'Card 2', cols: 1, rows: 1 },
        { title: 'Card 3', cols: 1, rows: 2 },
        { title: 'Card 4', cols: 1, rows: 1 }
      ];
    })
  );

  isAuthenticated: boolean;

  _htmlContent = '';

  get htmlContent(): string {
    return this._htmlContent;
  }


  set htmlContent(content: string) {
    this._htmlContent = content;
    // console.log('htmlContent->', content);
    // this.contentChanged.next(this._htmlContent);
    /*
    this.chapterChanged$.subscribe((value) => {
      // console.log('chapterChanged$->', value);
    });
    */
  }


  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private vcr: ViewContainerRef,
    private cus: CurrentUserService,
    private chapterService: ChapterService,
    private bookService: BookService,
    private autorService: AutorService) { }


  ngOnInit(): void {
    this.isAutor = false;
    this.isAuthenticated = this.cus.isAuthenticated;
    this.user = this.cus.user;
    this.isAdmin = this.cus.isAdmin;
    this.canSave = false;
    this.initFields();
    this.userChanged = this.cus.UserChanged.subscribe(newUser => {
      console.log('newUser->', newUser);
      this.user = newUser;
      this.isAdmin = this.cus.isAdmin;
      this.initFields();
    });

    this.chapterChanged = this.cus.SelectedChapterChanged.subscribe( newChapter => {
      console.log('newChapter->', newChapter);
    });

    
    /*
      this.chapterChanged$ = this.contentChanged.pipe(
        // wait 300ms after each keystroke before considering the term
        debounceTime(1000),
  
        // ignore new term if same as previous term
        distinctUntilChanged(),
  
        // switch to new search observable each time the term changes
        switchMap((val: string) => {
          // console.log('val= ', val);
  
          this.bchContentSave(val);
          return Observable.of(val);
        }),
      );
      */
    // this.bchContentSave();
    /*
    this.chapterChanged = this.contentChanged.subscribe(newChapter => {
      console.log('newChapter->', newChapter);
      this.getSelBook();
    });
  */

    this.breakpointObserverInit();
    // this.loadBookList();
  }

  ngOnDestroy(): void {
    clearTimeout(this.timeout);
    if (this.userChanged) { this.userChanged.unsubscribe(); }
    if (this.chapterChanged) { this.chapterChanged.unsubscribe(); }
    if (this.bpoObsHandset) { this.bpoObsHandset.unsubscribe(); }
    if (this.bpoObsTablet) { this.bpoObsTablet.unsubscribe(); }
    if (this.bpoObsWeb) { this.bpoObsWeb.unsubscribe(); }
    if (this.bpoObsHandset) { this.bpoObsHandset.unsubscribe(); }
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit -> ');
    if (this.chapter && this.chapter.bch_content) {
      this._htmlContent = this.chapter && this.chapter.bch_content ? this.chapter.bch_content : '';
    }
    console.log('editor= ', this.editor);
    // Clear the timeout if it has already been set.
    // This will prevent the previous task from executing
    // if it has been less than <MILLISECONDS>

    /*
  this.editor.registerOnChange(e => {
    console.log('onchange= ', e);
    this.contentChanged.next(e);
  });
     */

    /*
        Observable.fromEvent(this.editor.editorWrapper.nativeElement, 'innerHTML').pipe(
          // wait 300ms after each keystroke before considering the term
          debounceTime(10),
          // distinctUntilChanged(),
          // switch to new search observable each time the term changes
          switchMap((val: string) => {
            console.log('val= ', val);
            this.notTypingStuff();
            return this.chapter;
          })
        );
        /*
      this.input$.subscribe((val: string) => {
        console.log('val= ', val);
        this.notTypingStuff();
      });

      Observable.fromEvent(this.editor.nativeElement, 'input')
        .map((event: Event) => (<HTMLInputElement>event.target).value)
        .debounceTime(300)
        .distinctUntilChanged()
        .subscribe(data => this.notTypingStuff());
    */
  }

  //#region Action Methods

  onChange2(event) {
    // console.log('onChange2', this._htmlContent);
    this.canSave = true;
  }

  onBlur(event) {
    // console.log('blur ' + event);
  }

  onChange3(event) {
    // console.log('onChange3', this._htmlContent);
  }

  initFields() {

    this.book = this.cus.selectedBook;
    this.chapter = this.cus.selectedChapter;
    console.log('getSelBook-> chapter:', this.chapter);
    // this._htmlContent = this.chapter && this.chapter.bch_content ? this.chapter.bch_content : '';

    this.isAutor = this.book ? (this.book.userId === this.cus.user.id) : false;

  }



  onChange(newVal: string) {
    // console.log('onChange-> newVal', newVal);
    if (this.chapter) {
       this.contentChanged.next(this.chapter.bch_content);
    }
  }

  onContentSave() {
    console.log('onContentSave htmlContent->', this.htmlContent);
    this.bchContentSave(this.htmlContent);
  }

  bchContentSave(content: string) {
    if (this.chapter) {
      this.chapter.bch_content = content;
      // console.log('bchContentSave -> chapter: ', this.chapter);
      this.chapterService.save(this.chapter).subscribe((res: Array<ChapterModel>) => {
        console.log('chapterService.save->res:', res);
        if (res) {
          this.chapter = res[0];
          this.cus.selectedChapter = res[0];
          this._htmlContent = this.chapter && this.chapter.bch_content ? this.chapter.bch_content : '';
          this.canSave = false;
        }
      });
    }
  }

  //#endregion
  getSelBook() {

    if (!this.cus.isAuthenticated) {
      this.router.navigate(['/', BwtRoutes.Public]);
    } else {
      this.book = this.cus.selectedBook;
      console.log('getSelBook-> book:', this.book);
      this.chapter = this.cus.selectedChapter;
      console.log('getSelBook-> chapter:', this.chapter);
      // this._htmlContent = this.chapter && this.chapter.bch_content ? this.chapter.bch_content : '';
      // console.log('getSelBook-> _htmlContent:', this.htmlContent );
      this.isAutor = this.book ? (this.book.userId === this.cus.user.id) : false;
      console.log('getSelBook-> isAutor:', this.isAutor);
      if (this.book) {
        this.autorService.autor(this.book.userId)
          .subscribe((autorList: Array<AutorModel>) => {
            if (autorList) {
              this.autor = autorList[0];
            } else {
              this.autor = undefined;
            }
          });
      }
    }

  }


  breakpointObserverInit(): void {
    this.bpoObsHandset = this.breakpointObserver
      .observe([Breakpoints.Handset])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Handset viewport  mode');
          this.colspan4Card = 12;
          this.colspanGridMainBoard = 10;
          this.colspanGridMainRSide = 2;
        }
      });
    this.bpoObsTablet = this.breakpointObserver
      .observe([Breakpoints.Tablet])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Tablet viewport mode');
          this.colspan4Card = 6;
          this.colspanGridMainBoard = 9;
          this.colspanGridMainRSide = 3;
        }
      });

    this.bpoObsWeb = this.breakpointObserver
      .observe([Breakpoints.Web])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Web viewport mode');
          this.colspan4Card = 3;
          this.colspanGridMainBoard = 8;
          this.colspanGridMainRSide = 4;
        }
      });
    this.bpoObssmall = this.breakpointObserver
      .observe([Breakpoints.Small])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          // console.log('Matches Small viewport  mode');
          this.colspan4Card = 12;
          this.colspanGridMainBoard = 12;
          this.colspanGridMainRSide = 0;
        }
      });

  }

  updateFields(): void { }

  //#region Data
  /*
    loadBookList(): void {
      this.bookList = new Array<BookModel>();
      this.bookService.bookList()
        .subscribe((resp: Array<BookModel>) => {
          this.bookList = resp;
          console.log('loadBookList->', resp);
          this.updateFields();
        });

    }

    reloadBookList(): void {
      this.bookService.resetBookCache();
      this.loadBookList();
    }
  */
  //#endregion
}
