import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material/grid-list';
import { RoutedComponents, ChapterRoutingModule } from './routing.module';
import { SharedModule } from '../@share/shared.module';
import { BwtMaterialModule } from '../@share/material/material.module';
import { PageHeaderComponent } from './page-header/page-header.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    BwtMaterialModule,
    MatGridListModule,
    ChapterRoutingModule,
    AngularEditorModule,
  ],
  declarations: [
    RoutedComponents,
    PageHeaderComponent,
  ],
  exports: [
    PageHeaderComponent,
  ]
})
export class ChapterModule { }
